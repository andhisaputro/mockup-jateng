<?php

namespace App\Providers; 
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    { 
        Gate::define('superadmin', function($user) {
            return $user->role == 'superadmin';
        }); 

        Gate::define('parent', function($user) {
            return $user->role == 'parent';
        }); 

        Schema::defaultStringLength(191);
    }
}
