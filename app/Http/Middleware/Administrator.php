<?php

namespace App\Http\Middleware;
use App\Http\Models\SchoolYears;

use Closure;

class Administrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $years = SchoolYears::get();
        if(session("active_school_year") == null)
            session(["active_school_year" => SchoolYears::where("year_from",date("Y"))->first()->id]);

        // if(session("active_school_year_id"))
        $request->request->add(["school_years" => SchoolYears::get()]);
        return $next($request);
    }
}
