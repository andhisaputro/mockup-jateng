<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Libraries\Jwt\JWTHelper;
use Illuminate\Support\Facades\Cache;
use App\Http\Transformers\ResponseTransformer;
use Illuminate\Support\Facades\Route;

class FormValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    { 
        $route_name = Route::getCurrentRoute()->getName();
        $validation = $this->_selector($route_name); 

        $validator = app()->make('validator');
        $validate = $validator->make($request->all(),$validation);
        if($validate->fails()){
          return (new ResponseTransformer)->toJson(400,'Error Input', $validate->errors());
        }else {
          return $next($request);
        }
    }

    private function _selector($route){
      switch ($route) { 
        // AUTH
        case 'PostBrochureControllerstore':
            return [
              'phone' => 'required',
              'email' => 'required|email'
            ];
        break;
        case 'PostAdminOnlineStudentRegisterControllersubmitClass':
          return[
            'class_uuid' => 'required|exists:classes,id',
            'student_email' => 'required|email|unique:users,email',
          ];
        break;
        case 'PostAdminClassControllersubmitAdd':
          return[
            'level' => 'required|integer|between:1,12',
            'name' => 'required',
            'school_id' => 'required|exists:schools,id'
          ];
        break;
        default:
          return [];
      }
    }
}
