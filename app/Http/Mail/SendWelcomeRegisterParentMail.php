<?php

namespace App\Http\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendWelcomeRegisterParentMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $subject = '';
      if(env('APP_ENV') != 'production') 
        $subject .= '[Development]';

      $subject .= __("page.EMAIL.000002.001");

      return $this->view('email.confirm-register-parent')
      ->from(env('MAIL_FROM_ADDRESS'),__("page.company.000001"))
      ->subject($subject)
      ->with($this->data);
    }
} 