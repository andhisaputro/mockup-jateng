<?php

namespace App\Http\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendBrochureMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $subject = '';
      if(env('APP_ENV') != 'production') 
        $subject .= '[Development]';

      $subject .= __("custome.EMAIL.000001.001");

      return $this->view('email.send-brochure')
      ->from(env('MAIL_FROM_ADDRESS'),__("page.company.000001"))
      ->subject($subject)
      ->attach("https://smslingker.s3-ap-southeast-1.amazonaws.com/production/public/static/assets/docs/Brosur+Example+File.pdf")
      ->with($this->data);
    }
} 