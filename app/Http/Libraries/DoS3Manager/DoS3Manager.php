<?php namespace app\Http\Libraries\DoS3Manager;

use Illuminate\Support\Facades\Storage,  
    Illuminate\Contracts\Filesystem\Filesystem,
    App\Http\Models\UploadFiles;

use File;
use Intervention\Image\ImageManagerStatic as Image;

class DoS3Manager {

    private static $autoRatio = '';

    public function __construct(){
        
    }
 
    // === fuction Upload multy size ===
    static public function saveImage($image = null , $path ='web/assets/images',$thumbnail = false , $filename = null, $ratio = null , $access = 'public')
    {
        if (!file_exists('tmp/xsmall')) mkdir('tmp/xsmall', 0777, true); 
        if (!file_exists('tmp/small')) mkdir('tmp/small', 0777, true); 
        if (!file_exists('tmp/medium')) mkdir('tmp/medium', 0777, true); 
        if (!file_exists('tmp/large')) mkdir('tmp/large', 0777, true); 
        if (!file_exists('tmp/xlarge')) mkdir('tmp/xlarge', 0777, true);  

        $save = false;
                
        if($image == null){
            alert()->warning('Missing Image !');
            return back();
        }
        
        $filename		= ($filename==null) ? $image->getClientOriginalName() : $filename;

        $originalSize   = getimagesize($image);
        $sizeimg        = filesize($image);
        $mime           = $originalSize['mime'];

        if($thumbnail == true){

            $width = $originalSize[0];
            $height = $originalSize[1];
            try {
                $thumbnail = static::autoRatio($width,$height,$ratio);
                $tempPath = 'tmp';
                $deleteTemp = [];
                Storage::disk('spaces')->putFileAs($path, $image, $filename,$access);

                foreach ($thumbnail as $key => $size) {
                     
                    $newPath = $tempPath.'/'.$key;
                    $savePath = $tempPath.'/'.$key.'/'.$filename; 

                    $save_tmp = Image::make($image)->resize($size['width'], $size['height'])->save($savePath);

                    $save_tmp_path = new \SplFileInfo($save_tmp->dirname.'/'.$save_tmp->filename.'.'.$save_tmp->extension);
 
                    array_push($deleteTemp,$savePath);
                     
                    $save = Storage::disk('spaces')->putFileAs($path.'/'.$key, $save_tmp_path , $filename, $access);
                }
                
                
                foreach($deleteTemp as $value){
                    try {
                        // Storage::disk('spaces')->putFileAs($path.'/'.$key, $value, $filename, 'public');
                        if(!is_writable($value)){
                            alert()->warning('Permission denied delete file!');
                            return back();
                        }
                        unlink($value);
                    }
                    catch(Exception $e) {
                        alert()->warning('Permission denied delete file!');
                        return back();
                    }

                }

                return [
                    "file_path" => $path,
                    "file_name" => $filename
                ];

            }catch(Exception $e) {
                alert()->warning('Error');
                return back();
            }
        }
        
        return false;
    }


    static public function saveApplicationFile($file , $path , $filename , $access ){
    try {

        $save = Storage::disk('spaces')->putFileAs($path, $file, $filename , $access);
        
        return [
            "file_path" => $path,
            "file_name" => $filename
        ];

    }catch(Exception $e) {
        return (new MessageBag)->add('error',__('messages.409')); 
    }

        return false; 
    }

    
    private static function autoRatio($width,$height,$ratio=null,$autoResize=false)
    {
        $perbandingan=['xsmall'=>2,'small'=>5,'medium' => 10,'large'=>15,'xlarge'=>20];
        if($width <= 175){
            $value = 'xsmall';
        }elseif($width > 175 && $width <=375){
            $value = 'small';

        }elseif($width > 375 && $width <= 625){
            $value = 'medium';
        }
        elseif($width > 625 && $width <= 875){
            $value = 'large';
        }else{
            $value = 'xlarge';
        }
        $t = 100;
        $data = array();
        foreach($perbandingan as $name => $angka){
            if($autoResize == false){
                $data[$name]['width'] = ($perbandingan[$name]/$perbandingan['xsmall'])*$t;
            }
            else{
                $data[$name]['width'] = ($perbandingan[$name]/$perbandingan[$value])*$width;
            }
            $data[$name]['height'] = ($ratio === null ? $height/$width*$data[$name]['width'] : $data[$name]['width']/($ratio[0]/$ratio[1]));
        }
        return $data;
    }
}