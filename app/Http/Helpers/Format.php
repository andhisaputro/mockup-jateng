<?php

/* Init for global function
   Note : Don't put function related to models in here
*/ 

function uuid(){  
  return Ramsey\Uuid\Uuid::uuid4()->toString();
}

function date_time_format($date,$type = 'date'){
    if($type == 'date')
      return Carbon\Carbon::parse($date)->format('Y-m-d');
    
    if($type == 'date_time')
      return Carbon\Carbon::parse($date)->format('d, M Y H:i');

    if($type == 'time')
      return Carbon\Carbon::parse($date)->format('H:i');
}

function avaiLevels($type){
    if($type == 1)
      return [1,2,3,4,5,6];

    if($type == 1)
      return [7,8,9];

    if($type == 1)
      return [10,11,12];
}
