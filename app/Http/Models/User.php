<?php namespace App\Http\Models; 

use Illuminate\Auth\Authenticatable; 
// use Illuminate\Database\Eloquent\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class User extends Model implements AuthenticatableContract
{

    use SoftDeletes;

    use Authenticatable;
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'users';
    
    protected $dates = ['deleted_at'];

    protected $fillable = ['id','email','password','phone','first_name','last_name','otp','lang','username','temporary_token','temp_expired_at'];

    public static $messages = array();

    protected $casts = ['id' => 'string'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at','password','otp'];

    public static $rules = array(
      'phone'             => 'min:8|unique:users',
      'email'             => 'required|email|unique:users|max:191',      
      'confirm_password'  => 'required',
      'first_name'        => 'required|max:191'
    );

    public function UserDevice()
    {
        return $this->hasMany('App\Http\Models\UserDevices','user_id','id');
    } 

    public function Class()
    {
        return $this->belongsToMany('App\Http\Models\Classes','class_members','user_id','class_id')->withPivot('status','type');
    } 

    public function MemberClass()
    {
        return $this->hasMany('App\Http\Models\MemberClass','user_id','id');
    } 

    public function Child()
    {
        return $this->hasMany('App\Http\Models\User','parent_id','id');
    } 

    public function RegisterationForm()
    {
        return $this->hasMany('App\Http\Models\RegisteratioForm','parent_user_id','id');
    } 
}
