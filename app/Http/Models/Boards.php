<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Boards extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'boards';
    
    protected $fillable = [
      'id',
      'parent_id',
      'name',
      'description',
      'image_path',
      'image_file',
      'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at','password','otp'];

    protected $casts = ['id' => 'string'];

    public static $rules = array( 
      'id'                => 'required', 
      'name'              => 'required',
    );

    public $module = 'boards';
  
    public function BoardMembers(){
      return $this->hasMany('App\Http\Models\BoardMembers','board_id','id');
    }

    public function Host(){
      return $this->hasOne('App\Http\Models\BoardMembers','board_id','id')->where('type','=',1);
    }

    public function Member(){
      return $this->hasMany('App\Http\Models\BoardMembers','board_id','id');
    }
 
    public function Announcements(){
      return $this->hasMany('App\Http\Models\Announcements','board_id','id');
    }

    public function Project(){
      return $this->hasMany('App\Http\Models\BoardProjects','board_id','id');
    }
}
