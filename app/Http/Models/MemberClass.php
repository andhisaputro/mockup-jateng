<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
// use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
// use Illuminate\Database\Eloquent\SoftDeletes; 
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;

class MemberClass extends Model {

    // use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'class_members';
    protected $casts = ['id' => 'string'];

    protected $fillable = [
        "user_id",
        "class_id",
        "type"
    ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at']; 
   
}
