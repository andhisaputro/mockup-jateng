<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Schools extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'schools';
    protected $casts = ['id' => 'string'];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at']; 
 
    public function Grade(){
        return $this->hasOne('App\Http\Models\Grades','id','grade_id');
    }

    public function Class(){
        return $this->hasMany('App\Http\Models\Classes','school_id','id');
    }

}
