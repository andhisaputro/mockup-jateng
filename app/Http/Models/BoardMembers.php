<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class BoardMembers extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'board_members';
    
    protected $casts = ['id' => 'string'];

    protected $fillable = [
      'id',
      'user_id', 
      'board_id', 
      'status',
      'type'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at'];

    public static $rules = array( 
      'id'                    => 'required', 
      'user_id'               => 'required',
      'board_id'              => 'required',
    );

    public static $messages = array();

    public function User(){
      return $this->hasOne('App\Http\Models\User','id','user_id');
    }

    public function Title(){
      return $this->hasOne('App\Http\Models\BoardMemberTitles','id','board_member_title_id');
    }

    public function TypeDetail()
    {
        return $this->belongsTo('App\Http\Models\Types','type','foreign_key')->where('module','=','board_members');
    }

    public function StatusDetail()
    {
        return $this->belongsTo('App\Http\Models\Statuses','type','foreign_key')->where('module','=','board_members');
    }

    public function Attendance()
    {
        return $this->hasMany('App\Http\Models\Attendance','user_id','user_id');
    }

    public function Child()
    {
        return $this->hasMany('App\Http\Models\BoardMembers','parent_id','id');
    }

    public function Head()
    {
        return $this->hasMany('App\Http\Models\BoardMembers','id','parent_id');
    }

    public function Parent()
    {
        return $this->hasMany('App\Http\Models\BoardMembers','parent_id','parent_id');
    }
 
}
