<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes; 

class RegisteratioForm extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'registeration_forms';

    protected $casts = ['id' => 'string'];
 
    public $module   = 'registeration_forms'; 

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public $fillable = [
        "student_first_name",
        "student_last_name",
        "student_bod",
        "parent_first_name",
        "parent_last_name",
        "parent_email",
        "parent_handphone",
        "school_type",
        "school_grade",
        "type",
        "role",
        "school_id",
        "user_id"
    ]; 


    public function Parent()
    {
        return $this->hasOne('App\Http\Models\User','id','parent_user_id');
    }

    public function School(){
        return $this->hasOne('App\Http\Models\Schools','id','school_id');
    }

    public function User()
    {
        return $this->hasOne('App\Http\Models\User','id','user_id');
    }
    
}
