<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Classes extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'classes';
    protected $casts = ['id' => 'string'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at']; 
 
    public function School(){
        return $this->hasOne('App\Http\Models\Schools','id','school_id');
    }

    public function SchoolYears(){
        return $this->hasOne('App\Http\Models\SchoolYears','id','school_year_id');
    }
}
