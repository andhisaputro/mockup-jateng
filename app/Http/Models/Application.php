<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model
{
    use SoftDeletes;

    public $table = 'applications';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'email',
        'app_key',
        'app_secret',
        'name',
        'description',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'email' => 'string',
        'app_key' => 'string',
        'app_secret' => 'string',
        'name' => 'string',
        'description' => 'string',
        'status' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required|email|between:6,64|unique:applications,name',
        'app_key' => 'required|between:6,64',
        'app_secret' => 'required|between:6,1000',
        'name' => 'required|between:6,64',
        'description' => 'min:3',
        'status' => 'required|integer|in:1,2',
    ];

    /**
     * Validation messages
     *
     * @var array
     */
    public static $messages = [

    ];

}
