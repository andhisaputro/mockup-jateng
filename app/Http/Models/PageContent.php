<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class PageContent extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'page_contents';
     

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public $fillable = [
        'id',
        'slug',
        'text',
        'title',
        'image_path',
        'image_file',
        'type'
    ];

    protected $casts = ['id' => 'string'];
 
    public $module = 'page_contents';   

}
