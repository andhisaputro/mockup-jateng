<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Tasks extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'tasks';

    protected $casts = ['id' => 'string'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at']; 

    public function TaskStatus()
    {
        return $this->hasOne('App\Http\Models\BoardProjectStatuses','id','board_project_status_id');
    }
 
    public function User()
    {
        return $this->hasOne('App\Http\Models\User','id','user_id');
    }

    public function Logs(){
        return $this->hasMany('App\Http\Models\ChangeLogs','foreign_key_parent','id')->orderBy('created_at','DESC');
      }

    public function BoardProjects()
        {
            return $this->hasOne('App\Http\Models\BoardProjects','id','board_project_id');
        }
 
}
