<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\SoftDeletes; 
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model; 

class SchoolYears extends Model
{
    //
    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'school_years';
    protected $casts = ['id' => 'string'];
}
