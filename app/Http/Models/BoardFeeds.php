<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class BoardFeeds extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'board_feeds';
    
    protected $casts = ['id' => 'string'];
 
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at'];
  
    public static $messages = array();

    public function Attendance(){
      return $this->hasOne('App\Http\Models\Attendance','id','foreign_key');
    }

    public function Announcement(){
      return $this->hasOne('App\Http\Models\Announcements','id','foreign_key');
    }
  
  
 
}
