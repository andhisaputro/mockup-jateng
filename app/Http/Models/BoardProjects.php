<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class BoardProjects extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'board_projects';
    
    protected $casts = ['id' => 'string'];
 
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at']; 

    public static $messages = array();

    public function User()
    {
        return $this->hasOne('App\Http\Models\User','id','user_id');
    }

    public function Board()
    {
        return $this->hasOne('App\Http\Models\Boards','id','board_id');
    }

    public function BoardProjectStatus()
    {
        return $this->hasMany('App\Http\Models\BoardProjectStatuses','board_project_id','id');
    }

    public function FirstStatus()
    {
        return $this->hasOne('App\Http\Models\BoardProjectStatuses','board_project_id','id')->where('index',1);
    }
 
}
