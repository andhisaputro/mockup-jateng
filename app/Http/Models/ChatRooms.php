<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class ChatRooms extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'chat_rooms';
    
    protected $casts = ['id' => 'string'];
 
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at']; 

    public static $messages = array(); 
    
    protected $fillable = [
        'id',
        'foreign_key',
        'status',
        'type'
      ];

    public $module = 'chat_rooms';

    public function ChatMessage(){
      return $this->hasMany('App\Http\Models\ChatMessages','chat_room_id','id');
    }

    public function Board(){
      return $this->hasOne('App\Http\Models\Boards','id','foreign_key');
    }

    public function ChatMember(){
      return $this->hasMany('App\Http\Models\ChatMembers','chat_room_id','id');
    }
}
