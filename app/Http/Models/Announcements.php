<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Announcements extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'announcements';
    
    protected $casts = ['id' => 'string'];
 
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at']; 

    public static $messages = array();

    public function User(){
      return $this->hasOne('App\Http\Models\User','id','user_id');
    }
 
    public function TypeDetail()
    {
        return $this->belongsTo('App\Http\Models\Types','type','foreign_key')->where('module','=','announcements');
    }

    public function StatusDetail()
    {
        return $this->belongsTo('App\Http\Models\Statuses','type','foreign_key')->where('module','=','announcements');
    }
 
}
