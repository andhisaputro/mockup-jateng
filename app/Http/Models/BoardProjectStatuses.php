<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class BoardProjectStatuses extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'board_project_statuses';
    
    protected $casts = ['id' => 'string'];
 
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at']; 

    public static $messages = array();

    public function User()
    {
        return $this->hasOne('App\Http\Models\User','id','user_id');
    }

    public function BoardProject()
    {
        return $this->hasOne('App\Http\Models\BoardProjects','id','board_project_id');
    }

    public function ProjectTask(){
        return $this->hasMany('App\Http\Models\Tasks','board_project_status_id','id');
    }
 
}
