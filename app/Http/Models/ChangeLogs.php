<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class ChangeLogs extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'change_logs';
     

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $casts = ['id' => 'string'];
 
    public $module = 'change_logs';
 
    public function User()
    {
        return $this->hasOne('App\Http\Models\User','id','user_id');
    }    

}
