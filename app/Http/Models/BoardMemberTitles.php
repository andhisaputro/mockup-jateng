<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class BoardMemberTitles extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'board_member_titles';
    
    protected $casts = ['id' => 'string'];

    protected $fillable = ['id','name','board_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at'];

    public static $rules = array( 
    );

    public static $messages = array();
  
    public function TypeDetail()
    {
        return $this->belongsTo('App\Models\Types','type','foreign_key')->where('module','=','board_member_titles');
    }

    public function StatusDetail()
    {
        return $this->belongsTo('App\Models\Statuses','type','foreign_key')->where('module','=','board_member_titles');
    }
}
