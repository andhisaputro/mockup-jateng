<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class ChatMessages extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected     $table = 'chat_messages';

    protected     $casts = ['id' => 'string'];
 
    protected     $hidden = ['deleted_at'];  
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    public $module = 'chat_messages';

    public function User()
    {
        return $this->hasOne('App\Http\Models\User','id','user_id');
    }

    // In my model
    public function getNricAttribute($value)
    {
        return Crypt::decrypt($value);;
    }

    public function setNricAttribute($value)
    {
        $this->attributes['nric'] = Crypt::encrypt($value);
    }
}
