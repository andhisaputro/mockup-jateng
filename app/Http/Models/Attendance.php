<?php namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Attendance extends Model {

    use SoftDeletes; 
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'attendance';
    
    protected $casts = ['id' => 'string'];
 
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['deleted_at'];

    protected $fillable = [
        "id",
        "board_id",
        "user_id",
        "longitude",
        "latitude",
        "date_time",
        "created_at",
        "status",
        "type"
    ];

  
    public static $messages = array();

    public function User(){
      return $this->hasOne('App\Http\Models\User','id','user_id');
    }
 
    public function TypeDetail()
    {
        return $this->belongsTo('App\Http\Models\Types','type','foreign_key')->where('module','=','attendance');
    }

    public function StatusDetail()
    {
        return $this->belongsTo('App\Http\Models\Statuses','type','foreign_key')->where('module','=','attendance');
    }

    public function Board(){
        return $this->hasOne('App\Http\Models\Boards','id','board_id');
      }

    public function Child(){
    return $this->hasOne('App\Http\Models\Attendance','parent_id','id');
    }
 
}
