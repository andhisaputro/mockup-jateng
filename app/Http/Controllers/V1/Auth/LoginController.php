<?php
namespace App\Http\Controllers\V1\Auth;

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    { 
    }

    public function view(Request $request){
        try {
            Auth::logout();
            $data["title"]        = "Login";
            $data["active_menu"]  = "login";

            return view('landing-page.login',$data);
            
        } catch (\exception $exception) {
            return (new ResponseTransformer)->toJson(500, 'Internal Server Error', $exception->getMessage().", The exception was created on ".$exception->getFile()." line: " . $exception->getLine());
        } 
    }

    public function submit(Request $request){
        $validator = $request->validate([
            'email'     => ['required','email'],
            'password'  => ['required']
        ]);

        $loggedIn = \Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ]); 

        if($loggedIn){
            return $this->redirectAfterLogin($request);
        }else{
            return redirect("auth/login") 
            ->withErrors([
                "email"     => __("validation.attributes.password_email"),
                "password"  => __("validation.attributes.password_email")
            ]);
        }
    }

    private function redirectAfterLogin($request){
        $role = Auth::user()->role; 
        if(in_array($role,["parent","student","teacher"]))
            return redirect(url("user"));

        if(in_array($role,["superadmin"]))
            return redirect(url("admin"));
            
        return abort(403);
    }

    public function logout(Request $request){ 
        Auth::logout(); 
        if(Auth::user() == null)
            return redirect()->intended('auth/login');
    }
 
}
