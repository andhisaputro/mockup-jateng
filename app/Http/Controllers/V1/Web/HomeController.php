<?php
namespace App\Http\Controllers\V1\Web;

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;  

class HomeController extends Controller
{

    public function __construct()
    { 
    }

    public function index(Request $request){
        $data['title']          = 'Home Page';
        $data['active_menu']    = 'home';
        $data['destinasions']   =  $this->destinationModel();
        $data['top3accomodation'] =  [
                                        $this->hotelList(1)[0],
                                        $this->hotelList(2)[2],
                                        $this->hotelList(3)[1]
                                    ];
        $data['top3tours']      =  [
                                        $this->tourList(1)[0],
                                        $this->tourList(2)[0],
                                        $this->tourList(3)[0]
                                    ];
                                    // dd($data['top3tours']);
        return view('landing-page.home',$data);
    }

    public function DestinationDetail(Request $request){
        $data['title']          = 'Destination '.ucwords(strtolower($request->slug)).' - '. url('');
        $data['active_menu']    = 'home';
        $data['top3accomodation'] =  [
            $this->hotelList(1)[1],
            $this->hotelList(2)[2],
            $this->hotelList(3)[1]
        ];
        $data['top3tours']      =  [
            $this->tourList(1)[0],
            $this->tourList(2)[0],
            $this->tourList(3)[0]
        ];
        $data['destinasions']   =  $this->destinationModel();

        $data['destinasion'] = $data['destinasions'][$request->slug];
        $data['current_location'] = $data['destinasions'][$request->slug]["name"];

        return view('landing-page.destination-detail',$data);
    }

    private function destinationModel(){
        return [ 
            "Banjarnegara" => [
                "id"    => 1,
                "slug"    => "Banjarnegara",
                "name"    => "Banjarnegara",
                "description" => "Banjarnegara",
                "image"   => "https://img.posjateng.id/img/content/2019/09/26/8173/banjarnegara-masuk-peralihan-musim-0ZoJzJ4Nld.jpg",
                "images"  => [
                    [
                        "name" => "Event January",
                        "url"  => "https://img.posjateng.id/img/content/2019/09/26/8173/banjarnegara-masuk-peralihan-musim-0ZoJzJ4Nld.jpg"
                    ],
                    [
                        "name" => "Event Feb",
                        "url"  => "https://cdn.idntimes.com/content-images/community/2019/06/dataran-tinggi-dieng-711424abb816a68c861039f12b3f94d3_600x400.jpg"
                    ],
                    [
                        "name" => "Event March",
                        "url"  => "https://img-z.okeinfo.net/library/images/2018/07/08/id654xy1ipa5t3lz76k9_20178.jpg"
                    ],
                    [
                        "name" => "Event April",
                        "url"  => "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTxKescW3pI-mfgjWJGLzz-iN7YMhzTfas0kw&usqp=CAU"
                    ]
                ],
                "description" => "Banjarnegara (Hanacaraka:ꦏꦧꦸꦥꦠꦼꦤ꧀​ꦧꦚ꧀ꦗꦂꦤꦼꦒꦫ, Bahasa Banyumasan: Kabupatèn Banjarnegara) adalah sebuah kabupaten di Provinsi Jawa Tengah, Indonesia. Ibu kotanya juga bernama Banjarnegara. Kabupaten Banjarnegara terletak di antara 7° 12' - 7° 31' Lintang Selatan dan 109° 29' - 109° 45'50 Bujur Timur. Luas Wilayah Kabupaten Banjarnegara adalah 106.970,997 ha atau 3,10 % dari luas seluruh Wilayah Provinsi Jawa Tengah. Kabupaten ini berbatasan dengan Kabupaten Pekalongan dan Kabupaten Batang di sebelah utara, Kabupaten Wonosobo di sisi timur, Kabupaten Kebumen di sisi selatan, serta Kabupaten Banyumas dan Kabupaten Purbalingga di sebelah barat."
            ],
            
            "Semarang" => [
                "id"    => 2,
                "slug"   => "Semarang",
                "name"   => "Semarang",
                "description" => "Semarang",
                "image"  => "https://storage.googleapis.com/allindonesiatoursim/2017/02/semarang-004.jpg",
                "images"  => [
                    [
                        "name" => "Event January",
                        "url"  => "https://storage.googleapis.com/allindonesiatoursim/2017/02/semarang-004.jpg"
                    ],
                    [
                        "name" => "Event Feb",
                        "url"  => "https://lh3.googleusercontent.com/proxy/IlXpA_czLCK2IgNp6Oc84GaQzJ-MvBoYWQnHm4uQL55yXRYCLaLPAxeHsQ6s8argrBa_ZBSRTFRPLIALWwz-jImCxmOniOySBgvcrGPXcqpHe_lxREirFMAPiO-h2d-B0o5OstfNBiKdLGeYYIsKfGRgANcvGf0x9w"
                    ],
                    [
                        "name" => "Event March",
                        "url"  => "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSEaz9M-X3lCst3rZA5rhKco3NnjNlFNn3TOg&usqp=CAU"
                    ],
                    [
                        "name" => "Event April",
                        "url"  => "https://assets.pikiran-rakyat.com/crop/0x0:0x0/750x500/photo/2019/10/pLpEVvrE2I7eEZYGeRTqCPq3F51dUSeK8XhuDPXz.jpeg"
                    ]
                ],
                "description" => "Kota Semarang (Hanacaraka:ꦏꦸꦛ​ꦱꦼꦩꦫꦁ, Jawa: Kutha Semarang) adalah ibu kota Provinsi Jawa Tengah, Indonesia sekaligus kota metropolitan terbesar kelima di Indonesia sesudah Jakarta, Surabaya, Medan, dan Bandung.[4][5] Sebagai salah satu kota paling berkembang di Pulau Jawa, Kota Semarang mempunyai jumlah penduduk lebih dari 1,7 juta jiwa dan siang hari bisa mencapai 2 juta jiwa. Kawasan mega-urban Semarang yang tergabung dalam wilayah metropolitan Kedungsepur (Kendal, Demak, Ungaran, Kabupaten Semarang, Kota Salatiga, Kota Semarang dan Purwodadi, Kabupaten Grobogan) berpenduduk mencapai 7,3 juta jiwa, sekaligus sebagai wilayah metropolitan terpadat keempat, setelah Jabodetabek (Jakarta), Gerbangkertosusilo (Surabaya), dan Bandung Raya. Dalam beberapa tahun terakhir, perkembangan Semarang yang signifikan ditandai pula dengan munculnya beberapa gedung pencakar langit yang tersebar di penjuru kota. Perkembangan regional ini menunjukan peran strategis Kota Semarang terhadap roda perekonomian nasional. "
            ],
            
            "solo" => [
                "id"    => 3,
                "slug"   => "solo",
                "name"   => "Solo",
                "description" => "Solo",
                "image"  => "https://ungarannews.com/wp-content/uploads/2020/03/balaikota-solo.jpg",
                "images"  => [
                    [
                        "name" => "Event January",
                        "url"  => "https://ungarannews.com/wp-content/uploads/2020/03/balaikota-solo.jpg"
                    ],
                    [
                        "name" => "Event Feb",
                        "url"  => "https://s3-ap-southeast-1.amazonaws.com/magazine.job-like.com/magazine/wp-content/uploads/2018/06/08211655/Liburan-ke-Surakarta-Kunjungi-3-Tempat-Wisata-Seru-di-Solo-Ini-Ya.jpg"
                    ],
                    [
                        "name" => "Event March",
                        "url"  => "https://www.mymagz.net/wp-content/uploads/sites/8/2019/11/Alila-Solo-Exterior-01.jpg"
                    ],
                    [
                        "name" => "Event April",
                        "url"  => "https://1.bp.blogspot.com/-UgsvsUR8gw0/WBQ0ansBKYI/AAAAAAAAGLw/UcoF3Ffe6ho_TV8vaL6v0WNbJDPc41kCACLcB/s1600/monumen%2Bpers%2Bnasional.jpg"
                    ]
                ],
                "description" => "Kota Surakarta (Hanacaraka:ꦑꦸꦛꦯꦸꦫꦏꦂꦠ, Jawa: Kutha Surakarta), juga disebut Solo (ꦱꦭ), adalah wilayah otonom dengan status Kota di bawah Provinsi Jawa Tengah, Indonesia, dengan penduduk 519.587 jiwa (2019) dan kepadatan 11.798,06/km2.[1] Kota dengan luas 44,04 km2, ini berbatasan dengan Kabupaten Karanganyar dan Kabupaten Boyolali di sebelah Utara, Kabupaten Karanganyar dan Kabupaten Sukoharjo di sebelah Timur dan barat, dan Kabupaten Sukoharjo di sebelah Selatan.[2] Kota ini juga merupakan kota terbesar ketiga di pulau Jawa bagian Selatan setelah Bandung dan Malang menurut jumlah penduduk. Sisi Timur kota ini dilewati sungai yang terabadikan dalam salah satu lagu keroncong, Bengawan Solo. Bersama dengan Yogyakarta, Surakarta merupakan pewaris Kesultanan Mataram yang dipecah melalui Perjanjian Giyanti, pada tahun 1755. "
            ],
            
            "Magelang" => [
                "id"    => 4,
                "slug"   => "Magelang",
                "name"   => "Magelang",
                "description" => "Magelang",
                "image"  => "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTUEy7th6XleWiNfg9rsGSpl8qqBipPnY8pzw&usqp=CAU",
                "images"  => [
                    [
                        "name" => "Event January",
                        "url"  => "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTUEy7th6XleWiNfg9rsGSpl8qqBipPnY8pzw&usqp=CAU"
                    ],
                    [
                        "name" => "Event Feb",
                        "url"  => "https://www.goodnewsfromindonesia.id/uploads/post/large-shutterstock-1141713023-79aeece18b82d371e14e4351131dca5b.jpg"
                    ],
                    [
                        "name" => "Event March",
                        "url"  => "https://cdns.klimg.com/dream.co.id/resized/750x500/news/2019/12/13/124954/15-tempat-dan-obyek-wisata-alam-di-magelang-191213g_3x2.jpg"
                    ],
                    [
                        "name" => "Event April",
                        "url"  => "https://awsimages.detik.net.id/community/media/visual/2020/07/02/jembatan-jokowi-di-magelang_169.jpeg?w=780&q=90"
                    ]
                ],
                "description" => "Magelang (bahasa Jawa: ꦏꦧꦸꦥꦠꦺꦤ꧀​ꦩꦒꦼꦭꦁ, translit. Kabupatèn Magelang) adalah sebuah Kabupaten di provinsi Jawa Tengah. Ibu kota Kabupaten ini adalah Kota Mungkid. Kabupaten ini berbatasan dengan Kabupaten Temanggung dan Kabupaten Semarang di utara, Kabupaten Semarang, Kabupaten Boyolali dan Kabupaten Klaten di timur, Kabupaten Kulon Progo dan Kabupaten Sleman (Daerah Istimewa Yogyakarta), serta Kabupaten Purworejo di selatan, Kabupaten Wonosobo dan Kabupaten Temanggung di barat, serta mengelilingi wilayah Kota Magelang. Candi Borobudur, sebuah mahakarya peninggalan Dinasti Syailendra yang kini menjadi kebanggaan Indonesia dan dunia, berada di wilayah Kabupaten Magelang."
            ]
        ];
    }


    public function accomodationList(Request $request){
        $data['title']          = 'Accomodation List - '. url('');
        $data['active_menu']    = 'home'; 
        $data['hotel_list']     = $this->hotelList($request->input("destination_id",1));
        $data['destinasions']   = $this->destinationModel();

        return view('landing-page.accomodation-list',$data);
    }

    private function hotelList($hotel_id){
        $data["1"] = [
            [
                "id"  => 1,
                "hotel_id" => 1,
                "name" => "Surya Yudha Park Banjarnegara",
                "description" => "Surya Yudha Park Banjarnegara",
                "review" => 933,
                "star" => 3,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/10000780-1024x683-FIT_AND_TRIM-f5b46bc6520e1dfbcb7ed0fcc69864b2.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                "address" => "JL Raya Rejasa KM 1, Pusat Kota Banjarnegara, Banjarnegara, Jawa Tengah, Indonesia, 53482",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/10000780-1024x683-FIT_AND_TRIM-f5b46bc6520e1dfbcb7ed0fcc69864b2.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                    "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/10000780-1024x683-FIT_AND_TRIM-2957739dadbaa505f166226893d5392d.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                    "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/10000780-1024x683-FIT_AND_TRIM-e27ac7a79f752de6f2fa1f72c990c526.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                    "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/10000780-1024x683-FIT_AND_TRIM-96b4d0d64057fd79812597f125301d03.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Keripik Buah Banjarnegara Shopping Mall",
                        "sub_title" => "Shop & Gifts",
                        "icon"=> "https://ik.imagekit.io/tvlk/image/imageResource/2020/02/03/1580726726363-44a43118fd320526eea2f316e6346818.png?tr=q-75"
                    ], 
                    [
                        "title" => "Cafe de IJA",
                        "sub_title" => "Casual Dining",
                        "icon"=> "https://ik.imagekit.io/tvlk/image/imageResource/2020/02/03/1580726682257-19aa22dc1baf793a1d16ff6e406a7df1.png?tr=q-75"
                    ],
                    [
                        "title" => "Terminal Banjarnegara",
                        "sub_title" => "Pusat Transportasi",
                        "icon"=> "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360709952-36ad2c9515c7449b9cd10ce72c4fb859.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                        [
                            "title" => "Alun - Alun Wonosobo",
                            "sub_title" => "Taman & Kebun Binatang",
                            "icon"=> "https://ik.imagekit.io/tvlk/image/imageResource/2020/02/03/1580726698277-52a042c6eb44f739e2bdb404b5011588.png?tr=q-75"
                        ],
                        [
                            "title" => "Dieng Theater",
                            "sub_title" => "Seni & Sains",
                            "icon"=> "https://ik.imagekit.io/tvlk/image/imageResource/2020/02/03/1580726732409-4d47101e13345dffff6d8b08aa6ad7f0.png?tr=q-75"
                        ],
                        [
                            "title" => "Benteng Van der Wijck",
                            "sub_title" => "Situs Bersejarah",
                            "icon"=> "https://ik.imagekit.io/tvlk/image/imageResource/2020/02/03/1580726738977-de00890e4808408d5f0a539173b4cb48.png?tr=q-75"
                        ],
                        [
                            "title" => "Stasiun Gombong",
                            "sub_title" => "Pusat Transportasi",
                            "icon"=> "https://ik.imagekit.io/tvlk/image/imageResource/2020/02/03/1580726726363-44a43118fd320526eea2f316e6346818.png?tr=q-75"
                        ]
                    ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 109.6905592,
                    "lat"  => -7.38899
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "450.000",
                        "is_availble" => false,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10000780-1024x683-FIT_AND_TRIM-e27ac7a79f752de6f2fa1f72c990c526.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "540.000",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10000780-1024x683-FIT_AND_TRIM-2957739dadbaa505f166226893d5392d.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Executive",
                        "price" => "630.000",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10000780-3000x2000-FIT_AND_TRIM-30a8c98c2a06e92af2d2c96d5f1870b1.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ],
            [
                "id"  => 2,
                "hotel_id" => 1,
                "name" => "Guest House Bagoes 306",
                "description" => "Guest House Bagoes 306",
                "review" => 26,
                "star" => 1,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/20024857-fac55203abd5506dc73a548d693ba4a2.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                "address" => "Jl. Letjen S Parman No.135, Kedung Menjangan, Pusat Kota Purbalingga, Purbalingga, Jawa Tengah, Indonesia, 53314",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/20024857-fac55203abd5506dc73a548d693ba4a2.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/20024857-37c35c799ebbf67a523ef63b25af22b7.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/20024857-a732785e98170048f6e1241e950bda53.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Alun Alun Purbalingga",
                        "sub_title" => "Taman & Kebun Binatang",
                        "distance" => "39 m",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Terminal Bus Purbalingga",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Caramello Cafe",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Alun-Alun Purwokerto",
                        "sub_title" => "Taman & Kebun Binatang",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Air Terjun Baturaden",
                        "sub_title" => "Alam",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Benteng Van der Wijck",
                        "sub_title" => "Situs Bersejarah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Gombong",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 109.3751196,
                    "lat" => -7.4041962
                ],
                "rooms" => [
                    [
                        "name" => "Standard",
                        "price" => "375.000",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/20024857-37c35c799ebbf67a523ef63b25af22b7.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe Double Bed",
                        "price" => "450.000",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/20024857-d8b8f1f457b3daa2c6b4430f44f1344d.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe Twin Bed",
                        "price" => "450.000",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/20024857-bc7c054e9e91e2adf20e5f80ca24c5d6.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Family",
                        "price" => "550.000",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/20024857-1a8d09a6451b04fa61e6fbb218347d7f.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ],
            [
                "id"  => 3,
                "hotel_id" => 1,
                "name" => "Efik Villa Dieng",
                "description" => "Efik Villa Dieng",
                "review" => 13,
                "star" => 1,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/10026575-fdcb41f9aac6c9557b4db177c4f55775.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                "address" => "Sumberejo Batur Banjarnegara, Batur, Banjarnegara, Jawa Tengah, Indonesia, 53456",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/10026575-fdcb41f9aac6c9557b4db177c4f55775.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                    "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/10026575-f3dd36e18b848abeadfe24c5c727c456.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                    "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/10026575-5d4d88ee5300ca8377a4f385930d0e78.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Dieng Plateau",
                        "sub_title" => "Taman & Kebun Binatang",
                        "distance" => "39 m",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Rumah Makan Dieng",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Alun - Alun Wonosobo",
                        "sub_title" => "Taman & Kebun Binatang",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Dieng Theater",
                        "sub_title" => "Seni & Sains",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Terminal Mendolo",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 109.8354234,
                    "lat" => -7.208484,
                ],
                "rooms" => [
                    [
                        "name" => "Villa 1",
                        "price" => "1.800.000",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10026575-1500x1500-FIT_AND_TRIM-9bd3ab9452fb1156feef9e016ddda024.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ],
            [
                "id"  => 4,
                "hotel_id" => 1,
                "name" => "The Cabin Tanjung Hotel",
                "description" => "Hotel",
                "review" => 212,
                "star" => 2,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/67719874-1083x720-FIT_AND_TRIM-5e8b9550cc081f305f5ede918452a0b2.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                "address" => "Jalan Banyumas Km. 4 No. 2 Selomerto, Selomerto, Wonosobo, Jawa Tengah, Indonesia, 56361",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/67719874-1083x720-FIT_AND_TRIM-5e8b9550cc081f305f5ede918452a0b2.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                    "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/67719874-1083x720-FIT_AND_TRIM-c5947798b0d537ba6cc50fb99e9cd13c.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                    "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/67719874-1083x720-FIT_AND_TRIM-db1dab0ca19aea0ae5d2618eb47c6020.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit",
                    "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/67719874-1083x720-FIT_AND_TRIM-9ae1967d095e16880ed36764be0e040b.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Mie Ongklok Pak Muhadi",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Dieng Theater",
                        "sub_title" => "Seni & Sains",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Terminal Mendolo",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Dieng Theater",
                        "sub_title" => "Seni & Sains",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Taman Kyai Langgeng",
                        "sub_title" => "Taman & Kebun Binatang",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Kutoarjo",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Candi Borobudur",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 109.8923245,
                    "lat" => -7.3942526
                ],
                "rooms" => [
                    [
                        "name" => "Big Cabin",
                        "price" => "340.000",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/67719874-1083x720-FIT_AND_TRIM-705929aa5b67b219b32a38e245060705.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Small Cabin Twin",
                        "price" => "340.000",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/67719874-1083x720-FIT_AND_TRIM-a6972a23f72673974a30b2b078197be5.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ]
        ];

        $data["2"] = [
            [
                "id"  => 1,
                "hotel_id" => 2,
                "name" => "Oak Tree Emerald Semarang Managed by The Ascott Limited",
                "description" => "Oak Tree Emerald Semarang Managed by The Ascott Limited",
                "review" => 5,
                "star" => 3,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10000304-35a234306a7c204de50ef6d77e4e3d85.jpeg?tr=q-40,w-300,h-300&_src=imagekit",
                "address" => "Jalan Pasar Kembang No 21, Jalan Malioboro, Yogyakarta, Provinsi Yogyakarta, Indonesia, 55271",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-861x470-FIT_AND_TRIM-f4513f91e906f3e87e890c3f217c5e29.jpeg?tr=q-40,w-300,h-300&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Noodles Now Restaurant at Hotel Neo Malioboro",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Tugu Yogyakarta",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Pusat Dagadu & Batik Djokdja",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ],
                    [
                        "title" => "PT PLN Persero Unit Induk Pembangunan Jawa Bagian Tengah II",
                        "sub_title" => "Bisnis",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Universitas Gadjah Mada",
                        "sub_title" => "Pendidikan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Lempuyangan",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Monumen Tugu",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Malioboro Mall",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 110.3644376,
                    "lat" => -7.790639
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "525.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-2362x1574-FIT_AND_TRIM-7e645b8c3ab9793010dff8422a198c74.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "669.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-26bb37cdf307cf1019a36b7e79e137e3.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Corner Suite",
                        "price" => "1.005.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-a06569778bbc378bf0c7ff2637ecda7e.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ],
            [
                "id"  => 2,
                "hotel_id" => 2,
                "name" => "MG Suites Hotel Semarang",
                "description" => "MG Suites Hotel Semarang",
                "review" => 5,
                "star" => 3,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10009089-900x599-FIT_AND_TRIM-16cb7385fac4b9fb8262fb336b60b486.jpeg?tr=q-40,w-300,h-300&_src=imagekit",
                "address" => "Jalan Pasar Kembang No 21, Jalan Malioboro, Yogyakarta, Provinsi Yogyakarta, Indonesia, 55271",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-861x470-FIT_AND_TRIM-f4513f91e906f3e87e890c3f217c5e29.jpeg?tr=q-40,w-300,h-300&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Noodles Now Restaurant at Hotel Neo Malioboro",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Tugu Yogyakarta",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Pusat Dagadu & Batik Djokdja",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ],
                    [
                        "title" => "PT PLN Persero Unit Induk Pembangunan Jawa Bagian Tengah II",
                        "sub_title" => "Bisnis",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Universitas Gadjah Mada",
                        "sub_title" => "Pendidikan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Lempuyangan",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Monumen Tugu",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Malioboro Mall",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 110.3644376,
                    "lat" => -7.790639
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "525.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-2362x1574-FIT_AND_TRIM-7e645b8c3ab9793010dff8422a198c74.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "669.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-26bb37cdf307cf1019a36b7e79e137e3.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Corner Suite",
                        "price" => "1.005.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-a06569778bbc378bf0c7ff2637ecda7e.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ],
            [
                "id"  => 3,
                "hotel_id" => 2,
                "name" => "Louis Kienne Hotel Simpang Lima",
                "description" => "Louis Kienne Hotel Simpang Lima",
                "review" => 5,
                "star" => 2,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007798-1455x912-FIT_AND_TRIM-1c3e04bc758b5da905ca17ba67b443d9.jpeg?tr=q-40,w-300,h-300&_src=imagekit",
                "address" => "Jalan Pasar Kembang No 21, Jalan Malioboro, Yogyakarta, Provinsi Yogyakarta, Indonesia, 55271",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-861x470-FIT_AND_TRIM-f4513f91e906f3e87e890c3f217c5e29.jpeg?tr=q-40,w-300,h-300&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Noodles Now Restaurant at Hotel Neo Malioboro",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Tugu Yogyakarta",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Pusat Dagadu & Batik Djokdja",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ],
                    [
                        "title" => "PT PLN Persero Unit Induk Pembangunan Jawa Bagian Tengah II",
                        "sub_title" => "Bisnis",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Universitas Gadjah Mada",
                        "sub_title" => "Pendidikan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Lempuyangan",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Monumen Tugu",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Malioboro Mall",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 110.3644376,
                    "lat" => -7.790639
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "525.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-2362x1574-FIT_AND_TRIM-7e645b8c3ab9793010dff8422a198c74.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "669.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-26bb37cdf307cf1019a36b7e79e137e3.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Corner Suite",
                        "price" => "1.005.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-a06569778bbc378bf0c7ff2637ecda7e.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ],
            [
                "id"  => 5,
                "hotel_id" => 2,
                "name" => "Whiz Hotel Pemuda Semarang",
                "description" => "Whiz Hotel Pemuda Semarang",
                "review" => 5,
                "star" => 1,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10000394-92e9ea65658311b230075653a8ec0f23.jpeg?tr=q-40,w-300,h-300&_src=imagekit",
                "address" => "Jalan Pasar Kembang No 21, Jalan Malioboro, Yogyakarta, Provinsi Yogyakarta, Indonesia, 55271",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-861x470-FIT_AND_TRIM-f4513f91e906f3e87e890c3f217c5e29.jpeg?tr=q-40,w-300,h-300&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Noodles Now Restaurant at Hotel Neo Malioboro",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Tugu Yogyakarta",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Pusat Dagadu & Batik Djokdja",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ],
                    [
                        "title" => "PT PLN Persero Unit Induk Pembangunan Jawa Bagian Tengah II",
                        "sub_title" => "Bisnis",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Universitas Gadjah Mada",
                        "sub_title" => "Pendidikan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Lempuyangan",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Monumen Tugu",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Malioboro Mall",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 110.3644376,
                    "lat" => -7.790639
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "525.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-2362x1574-FIT_AND_TRIM-7e645b8c3ab9793010dff8422a198c74.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "669.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-26bb37cdf307cf1019a36b7e79e137e3.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Corner Suite",
                        "price" => "1.005.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-a06569778bbc378bf0c7ff2637ecda7e.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ]
        ];

        $data["3"] = [
            [
                "id"  => 1,
                "hotel_id" => 3,
                "name" => "Lorin Solo Hotel",
                "description" => "Lorin Solo Hotel",
                "review" => 5,
                "star" => 3,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10000350-1181x787-FIT_AND_TRIM-b559156941bea12af7b271fca8a757d6.jpeg?tr=q-40,w-300,h-300&_src=imagekit",
                "address" => "Jalan Pasar Kembang No 21, Jalan Malioboro, Yogyakarta, Provinsi Yogyakarta, Indonesia, 55271",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-861x470-FIT_AND_TRIM-f4513f91e906f3e87e890c3f217c5e29.jpeg?tr=q-40,w-300,h-300&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Noodles Now Restaurant at Hotel Neo Malioboro",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Tugu Yogyakarta",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Pusat Dagadu & Batik Djokdja",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ],
                    [
                        "title" => "PT PLN Persero Unit Induk Pembangunan Jawa Bagian Tengah II",
                        "sub_title" => "Bisnis",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Universitas Gadjah Mada",
                        "sub_title" => "Pendidikan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Lempuyangan",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Monumen Tugu",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Malioboro Mall",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 110.3644376,
                    "lat" => -7.790639
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "525.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-2362x1574-FIT_AND_TRIM-7e645b8c3ab9793010dff8422a198c74.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "669.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-26bb37cdf307cf1019a36b7e79e137e3.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Corner Suite",
                        "price" => "1.005.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-a06569778bbc378bf0c7ff2637ecda7e.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ],
            [
                "id"  => 2,
                "hotel_id" => 3,
                "name" => "Syariah Hotel Solo",
                "description" => "Syariah Hotel Solo",
                "review" => 5,
                "star" => 2,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/10000353-445240ddac12d8548c117ee1f1bcbde5.jpeg?tr=q-40,w-300,h-300&_src=imagekit",
                "address" => "Jalan Pasar Kembang No 21, Jalan Malioboro, Yogyakarta, Provinsi Yogyakarta, Indonesia, 55271",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-861x470-FIT_AND_TRIM-f4513f91e906f3e87e890c3f217c5e29.jpeg?tr=q-40,w-300,h-300&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Noodles Now Restaurant at Hotel Neo Malioboro",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Tugu Yogyakarta",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Pusat Dagadu & Batik Djokdja",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ],
                    [
                        "title" => "PT PLN Persero Unit Induk Pembangunan Jawa Bagian Tengah II",
                        "sub_title" => "Bisnis",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Universitas Gadjah Mada",
                        "sub_title" => "Pendidikan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Lempuyangan",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Monumen Tugu",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Malioboro Mall",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 110.3644376,
                    "lat" => -7.790639
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "525.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-2362x1574-FIT_AND_TRIM-7e645b8c3ab9793010dff8422a198c74.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "669.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-26bb37cdf307cf1019a36b7e79e137e3.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Corner Suite",
                        "price" => "1.005.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-a06569778bbc378bf0c7ff2637ecda7e.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ],
            [
                "id"  => 3,
                "hotel_id" => 3,
                "name" => "Alila Solo",
                "description" => "Alila Solo",
                "review" => 5,
                "star" => 1,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10005325-b992eb8f35d275a8ceb15f958845ba3e.jpeg?tr=q-40,w-300,h-300&_src=imagekit",
                "address" => "Jalan Pasar Kembang No 21, Jalan Malioboro, Yogyakarta, Provinsi Yogyakarta, Indonesia, 55271",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-861x470-FIT_AND_TRIM-f4513f91e906f3e87e890c3f217c5e29.jpeg?tr=q-40,w-300,h-300&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Noodles Now Restaurant at Hotel Neo Malioboro",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Tugu Yogyakarta",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Pusat Dagadu & Batik Djokdja",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ],
                    [
                        "title" => "PT PLN Persero Unit Induk Pembangunan Jawa Bagian Tengah II",
                        "sub_title" => "Bisnis",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Universitas Gadjah Mada",
                        "sub_title" => "Pendidikan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Lempuyangan",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Monumen Tugu",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Malioboro Mall",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 110.3644376,
                    "lat" => -7.790639
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "525.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-2362x1574-FIT_AND_TRIM-7e645b8c3ab9793010dff8422a198c74.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "669.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-26bb37cdf307cf1019a36b7e79e137e3.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Corner Suite",
                        "price" => "1.005.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-a06569778bbc378bf0c7ff2637ecda7e.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ],
            [
                "id"  => 5,
                "hotel_id" => 3,
                "name" => "Solo Paragon Hotel & Residences",
                "description" => "Solo Paragon Hotel & Residences",
                "review" => 5,
                "star" => 3,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/10003193-3000x2000-FIT_AND_TRIM-a39256ae8f8217c22ac6be784ea61ded.jpeg?tr=q-40,w-300,h-300&_src=imagekit",
                "address" => "Jalan Pasar Kembang No 21, Jalan Malioboro, Yogyakarta, Provinsi Yogyakarta, Indonesia, 55271",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-861x470-FIT_AND_TRIM-f4513f91e906f3e87e890c3f217c5e29.jpeg?tr=q-40,w-300,h-300&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Noodles Now Restaurant at Hotel Neo Malioboro",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Tugu Yogyakarta",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Pusat Dagadu & Batik Djokdja",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ],
                    [
                        "title" => "PT PLN Persero Unit Induk Pembangunan Jawa Bagian Tengah II",
                        "sub_title" => "Bisnis",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Universitas Gadjah Mada",
                        "sub_title" => "Pendidikan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Lempuyangan",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Monumen Tugu",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Malioboro Mall",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 110.3644376,
                    "lat" => -7.790639
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "525.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-2362x1574-FIT_AND_TRIM-7e645b8c3ab9793010dff8422a198c74.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "669.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-26bb37cdf307cf1019a36b7e79e137e3.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Corner Suite",
                        "price" => "1.005.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-a06569778bbc378bf0c7ff2637ecda7e.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ]
        ];

        $data["4"] = [
            [
                "id"  => 1,
                "hotel_id" => 4,
                "name" => "The Joglo Family Hotel",
                "description" => "The Joglo Family Hotel",
                "type" => "Guest house",
                "review" => 5,
                "star" => 2,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10002860-868x1024-FIT_AND_TRIM-f0f9306c6aed589fe36d2b2e3d654bb9.jpeg?tr=q-40,w-300,h-300&_src=imagekit",
                "address" => "Jalan Pasar Kembang No 21, Jalan Malioboro, Yogyakarta, Provinsi Yogyakarta, Indonesia, 55271",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-861x470-FIT_AND_TRIM-f4513f91e906f3e87e890c3f217c5e29.jpeg?tr=q-40,w-300,h-300&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Noodles Now Restaurant at Hotel Neo Malioboro",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Tugu Yogyakarta",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Pusat Dagadu & Batik Djokdja",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ],
                    [
                        "title" => "PT PLN Persero Unit Induk Pembangunan Jawa Bagian Tengah II",
                        "sub_title" => "Bisnis",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Universitas Gadjah Mada",
                        "sub_title" => "Pendidikan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Lempuyangan",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Monumen Tugu",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Malioboro Mall",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 110.3644376,
                    "lat" => -7.790639
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "525.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-2362x1574-FIT_AND_TRIM-7e645b8c3ab9793010dff8422a198c74.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "669.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-26bb37cdf307cf1019a36b7e79e137e3.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Corner Suite",
                        "price" => "1.005.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-a06569778bbc378bf0c7ff2637ecda7e.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ],
            [
                "id"  => 2,
                "hotel_id" => 4,
                "name" => "The Amata Borobudur Resort",
                "description" => "Amata Borobudur Resort",
                "type" => "Guest house",
                "review" => 5,
                "star" => 3,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007041-6fe62b2bd851dbdbe62f26194b0aa9d0.jpeg?tr=q-40,w-300,h-300&_src=imagekit",
                "address" => "Jalan Pasar Kembang No 21, Jalan Malioboro, Yogyakarta, Provinsi Yogyakarta, Indonesia, 55271",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-861x470-FIT_AND_TRIM-f4513f91e906f3e87e890c3f217c5e29.jpeg?tr=q-40,w-300,h-300&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Noodles Now Restaurant at Hotel Neo Malioboro",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Tugu Yogyakarta",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Pusat Dagadu & Batik Djokdja",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ],
                    [
                        "title" => "PT PLN Persero Unit Induk Pembangunan Jawa Bagian Tengah II",
                        "sub_title" => "Bisnis",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Universitas Gadjah Mada",
                        "sub_title" => "Pendidikan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Lempuyangan",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Monumen Tugu",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Malioboro Mall",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 110.3644376,
                    "lat" => -7.790639
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "525.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-2362x1574-FIT_AND_TRIM-7e645b8c3ab9793010dff8422a198c74.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "669.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-26bb37cdf307cf1019a36b7e79e137e3.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Corner Suite",
                        "price" => "1.005.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-a06569778bbc378bf0c7ff2637ecda7e.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ],
            [
                "id"  => 3,
                "hotel_id" => 4,
                "name" => "Hotel Ahava",
                "description" => "Hotel Ahava",
                "type" => "Hotel",
                "review" => 5,
                "star" => 3,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/10012502-0dcb26d73fc7405dc1b6968d2702ccc4.jpeg?tr=q-40,w-300,h-300&_src=imagekit",
                "address" => "Jalan Pasar Kembang No 21, Jalan Malioboro, Yogyakarta, Provinsi Yogyakarta, Indonesia, 55271",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-861x470-FIT_AND_TRIM-f4513f91e906f3e87e890c3f217c5e29.jpeg?tr=q-40,w-300,h-300&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Noodles Now Restaurant at Hotel Neo Malioboro",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Tugu Yogyakarta",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Pusat Dagadu & Batik Djokdja",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ],
                    [
                        "title" => "PT PLN Persero Unit Induk Pembangunan Jawa Bagian Tengah II",
                        "sub_title" => "Bisnis",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Universitas Gadjah Mada",
                        "sub_title" => "Pendidikan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Lempuyangan",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Monumen Tugu",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Malioboro Mall",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 110.3644376,
                    "lat" => -7.790639
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "525.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-2362x1574-FIT_AND_TRIM-7e645b8c3ab9793010dff8422a198c74.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "669.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-26bb37cdf307cf1019a36b7e79e137e3.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Corner Suite",
                        "price" => "1.005.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-a06569778bbc378bf0c7ff2637ecda7e.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ],
            [
                "id"  => 4,
                "hotel_id" => 4,
                "name" => "Atria Hotel Magelang",
                "description" => "Atria Hotel Magelang",
                "type" => "Hotel",
                "review" => 5,
                "star" => 3,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10000196-305e616cfcd7f698e4e45619150e969c.jpeg?tr=q-40,w-300,h-300&_src=imagekit",
                "address" => "Jalan Pasar Kembang No 21, Jalan Malioboro, Yogyakarta, Provinsi Yogyakarta, Indonesia, 55271",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-861x470-FIT_AND_TRIM-f4513f91e906f3e87e890c3f217c5e29.jpeg?tr=q-40,w-300,h-300&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Noodles Now Restaurant at Hotel Neo Malioboro",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Tugu Yogyakarta",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Pusat Dagadu & Batik Djokdja",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ],
                    [
                        "title" => "PT PLN Persero Unit Induk Pembangunan Jawa Bagian Tengah II",
                        "sub_title" => "Bisnis",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Universitas Gadjah Mada",
                        "sub_title" => "Pendidikan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Lempuyangan",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Monumen Tugu",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Malioboro Mall",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 110.3644376,
                    "lat" => -7.790639
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "525.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-2362x1574-FIT_AND_TRIM-7e645b8c3ab9793010dff8422a198c74.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "669.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-26bb37cdf307cf1019a36b7e79e137e3.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Corner Suite",
                        "price" => "1.005.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-a06569778bbc378bf0c7ff2637ecda7e.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ],
            [
                "id"  => 4,
                "hotel_id" => 4,
                "name" => "The Oxalis Regency Hotel",
                "description" => "The Oxalis Regency Hotel",
                "type" => "Hotel",
                "review" => 5,
                "star" => 3,
                "image" => "https://ik.imagekit.io/tvlk/apr-asset/guys1L+Yyer9kzI3sp-pb0CG1j2bhflZGFUZOoIf1YOBAm37kEUOKR41ieUZm7ZJ/traveloka/hotel/asset/10001713-1100x736-FIT_AND_TRIM-24a02572dba65c453707af1fc1f3dbbc.jpeg?tr=q-40,w-300,h-300&_src=imagekit",
                "address" => "Jalan Pasar Kembang No 21, Jalan Malioboro, Yogyakarta, Provinsi Yogyakarta, Indonesia, 55271",
                "images" => [
                    "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-861x470-FIT_AND_TRIM-f4513f91e906f3e87e890c3f217c5e29.jpeg?tr=q-40,w-300,h-300&_src=imagekit"
                ],
                "nearby_places" => [
                    [
                        "title" => "Noodles Now Restaurant at Hotel Neo Malioboro",
                        "sub_title" => "Casual Dining",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360715852-a1a8ff42fed875bbf7df907c9f8f22e9.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Tugu Yogyakarta",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ],
                    [
                        "title" => "Pusat Dagadu & Batik Djokdja",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360743908-da1254fde1f0767c7ba2f4b9e972f4c2.png?tr=q-75"
                    ],
                    [
                        "title" => "PT PLN Persero Unit Induk Pembangunan Jawa Bagian Tengah II",
                        "sub_title" => "Bisnis",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ]
                ],
                "popular_area" => [
                    [
                        "title" => "Universitas Gadjah Mada",
                        "sub_title" => "Pendidikan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360782133-3dfe83e9b5f6686958161374ac9d86af.png?tr=q-75"
                    ],
                    [
                        "title" => "Stasiun Lempuyangan",
                        "sub_title" => "Pusat Transportasi",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360810981-9c87e83b3b5aa0807e3a823fbb9a107b.png?tr=q-75"
                    ],
                    [
                        "title" => "Monumen Tugu",
                        "sub_title" => "Pemandangan & Bangunan",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360805683-5899121c8828f597fdd407909d2d3d73.png?tr=q-75"
                    ],
                    [
                        "title" => "Malioboro Mall",
                        "sub_title" => "Toko & Hadiah",
                        "icon" => "https://ik.imagekit.io/tvlk/image/imageResource/2019/04/04/1554360738177-1056069448e7097b8a8cdbab9398fe14.png?tr=q-75"
                    ]
                ],
                "public_facility" => [
                    "Parking",
                    "Coffee shop",
                    "Elevator",
                    "Restaurant",
                    "Breakfast restaurant",
                    "Dinner restaurant",
                    "Lunch restaurant",
                    "Room service",
                    "Safety deposit box",
                    "WiFi in public area"
                ],
                "general_facility" => [
                    "AC",
                    "Ballroom",
                    "Banquet",
                    "Connecting rooms",
                    "Family room",
                    "Non-smoking room",
                    "Swimming pool",
                    "Smoking area"
                ],
                "location" => [
                    "long" => 110.3644376,
                    "lat" => -7.790639
                ],
                "rooms" => [
                    [
                        "name" => "Superior",
                        "price" => "525.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-2362x1574-FIT_AND_TRIM-7e645b8c3ab9793010dff8422a198c74.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Deluxe",
                        "price" => "669.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-26bb37cdf307cf1019a36b7e79e137e3.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ],
                    [
                        "name" => "Corner Suite",
                        "price" => "1.005.600",
                        "is_availble" => true,
                        "image" => "https://ik.imagekit.io/tvlk/apr-asset/dgXfoyh24ryQLRcGq00cIdKHRmotrWLNlvG-TxlcLxGkiDwaUSggleJNPRgIHCX6/hotel/asset/10007119-4000x2666-FIT_AND_TRIM-a06569778bbc378bf0c7ff2637ecda7e.jpeg?tr=q-40,c-at_max,w-740,h-500&_src=imagekit"
                    ]
                ]
            ]
        ];

        return $data[$hotel_id];
    }

    public function accomodationDetail(Request $request){
        $data['title']          = 'Accomodation Detail - '.url('');
        $data['active_menu']    = 'home'; 
        $data['hotel_list']     = $this->hotelList($request->input("destination_id",1));
        $data['hotel_detail']   =  $this->hotelList($request->destination_id)[$request->hotel_id - 1];  
        return view('landing-page.accomodation-detail',$data);
    }

    public function accomodationbooking(Request $request){
        $data['title']          = 'Accomodation Booking - '.url('');
        $data['active_menu']    = 'home';
        $data['hotel_detail']   =  $this->hotelList($request->destination_id)[$request->hotel_id - 1];  
        // dd($data['hotel_detail']);
        return view('landing-page.accomodation-booking',$data);
    }

    public function toursList(Request $request){
        $data['title']          = 'Tours List - '. url('');
        $data['active_menu']    = 'home';
        $destination_id         = $request->input("destination_id",false);
        $data['tour_list']      = ($destination_id) ? $this->tourList($destination_id) : [$this->tourList(1)[0],$this->tourList(2)[0],$this->tourList(3)[0],$this->tourList(4)[0] ];
        $data['destinasions']   = $this->destinationModel();

        return view('landing-page.tour-list',$data);
    }
    
    public function tourDetail(Request $request){
        $data['title']          = 'Tours Detail - '.url('');
        $data['active_menu']    = 'home'; 
        $data['tour_list']     = $this->tourList($request->input("destination_id",1));
        $tourlist = json_decode(json_encode($this->tourList($request->destination_id)));
        foreach ($tourlist as $row) {
            if($request->tour_id==$row->id){
                $data['tour_detail'] = $row;
                break;
            }
        }
        return view('landing-page.tours-detail',$data);
    }

    public function tourbooking(Request $request){
        $data['title']          = 'Tours Join - '.url('');
        $data['active_menu']    = 'home';
        $tourlist = json_decode(json_encode($this->tourList($request->destination_id)));
        foreach ($tourlist as $row) {
            if($request->tour_id==$row->id){
                $data['tour_detail'] = $row;
                break;
            }
        }
        return view('landing-page.tours-booking',$data);
    }

    private function tourList($destination_id){
        $data["1"] = [
            [
                "id"  => 1,
                "destination_id" => 1,
                "name" => "3H2M Dieng Plateau",
                "summary" => "Jelajahi Dataran Tinggi Dieng dalam 3 hari 2 malam, Lihat indahnya pesona alam Dieng seperti Telaga Warna, Batu Rapatan Angin, dan Kawah Sikidang, Nikmati panorama matahari terbit di Bukit Sikunir.",
                "tags" => ["Wisata Alam"],
                "review" => 5,
                "price" => "Rp 750.000",
                "images" => [
                    "https://ik.imagekit.io/tvlk/xpe-asset/dsIfD0QxFcgaDmB6sQchobk5CmBu9PzsWhwFXGFxJ179jzSxIGG5kZNhhHY-p7nw/xxt/experience/image/shutterstock-1279913059-dieng-jpg-1080x720-FIT-bab82e42363623c87071ec40f8764598.jpeg?_src=imagekit&tr=q-60,c-at_max,w-720,h-512",
                    "https://ik.imagekit.io/tvlk/xpe-asset/dsIfD0QxFcgaDmB6sQchobk5CmBu9PzsWhwFXGFxJ179jzSxIGG5kZNhhHY-p7nw/xxt/experience/image/shutterstock-354986063-dieng-jpg-1079x720-FIT-503291be5d8e3bbe8e138285fba31023.jpeg?_src=imagekit&tr=q-60,c-at_max,w-296,h-244",
                    "https://ik.imagekit.io/tvlk/xpe-asset/dsIfD0QxFcgaDmB6sQchobk5CmBu9PzsWhwFXGFxJ179jzSxIGG5kZNhhHY-p7nw/xxt/experience/image/shutterstock-1012377007-dieng-jpg-1087x720-FIT-b300d466bcc69ca745c3b1afd6897ec2.jpeg?_src=imagekit&tr=q-60,c-at_max,w-296,h-244",
                    "https://ik.imagekit.io/tvlk/xpe-asset/dsIfD0QxFcgaDmB6sQchobk5CmBu9PzsWhwFXGFxJ179jzSxIGG5kZNhhHY-p7nw/xxt/experience/image/shutterstock-1279913080-dieng-jpg-1080x720-FIT-5b5847b5375ab26f295e65d7579b7036.jpeg?_src=imagekit&tr=q-60,c-at_max,w-296,h-244"
                ],
                "location" => "Dieng, Banjarnegara",
                "description"=> "Pengalaman yang Menanti Anda Pernah dengar kata 'Kaldera' sebelumnya? Mungkin terdengar seperti nama camilan asal Mexico, tapi sebenarnya Kaldera adalah kawah yang muncul karena letupan dari gunung berapi, dan Indonesia juga memilikinya, lho! Dapatkan kesempatan untuk menjelajahi Dataran Tinggi Dieng dengan tur selama 3 hari dan 2 malam ini. Kaldera paling terkenal di Indonesia ini sangat kaya akan pesona alam dan warisan kebudayaan. Untuk Anda yang menyukai petualangan, Anda untuk menjelajahinya! Bersama tur ini, bersiaplah untuk mengeksplorasi Dataran Tinggi Dieng, mulai dari Kawah Sikidang hingga Telaga Warna! Cari tahu lebih banyak tentang budaya dan sejarah lokal, kemudian lanjutkan perjalanan Anda ke Bukit Sikunir, desa tertinggi di Jawa.",
                "iteneraries" => [
                    [
                        "title" => "Hari 1",
                        "summaries" => [
                            [
                                "time" => "2020-06-01 06:00:00",
                                "point" => "Bertemu di meeting point"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Menuju ke Wonosobo"
                            ]
                        ]
                    ],
                    [
                        "title" => "Hari 2",
                        "summaries" => [
                            [
                                "time" => "2020-06-01 06:00:00",
                                "point" => "Tiba di Wonosobo"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Menuju ke dataran Dieng, beristirahat sejenak di Gardu Pandang Tieng (opsional)"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Check-in, beristirahat & waktu bebas"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Menjelajahi area Candi Arjuna & Museum Kaliasa"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Menjelajahi Dieng Plateau Theatre, Telaga Warna, Batu Ratapan Angin dan Kawah Sikidang"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Kembali ke hotel, makan malam, waktu bebas & beristirahat"
                            ]
                        ]
                    ],
                    [
                        "title" => "Hari 3",
                        "summaries" => [
                            [
                                "time" => "2020-06-01 06:00:00",
                                "point" => "Wake up call & bersiap-siap"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Trekking, menikmati matahari terbit di Bukit Sikunir (desa tertinggi di Pulau Jawa)"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Kembali ke hotel"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Tiba di hotel, sarapan, check out dan menuju Wonosobo"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Kembali ke Jakarta"
                            ],
                        ]
                    ]
                ]
            ]
        ];

        $data["2"] = [
            [
                "id"  => 13,
                "destination_id" => 2,
                "name" => "3H2M Semarang",
                "summary" => "Habiskan hari yang menyenangkan di atraksi trampolin satu-satunya di kota Semarang, Jelajahi berbagai permainan lainnya seperti Arcade Ball Arena, Slam Dunk Arena, dan masih banyak lagi, Biarkan si kecil melatih ketangkasannya di area Ninja Warrior, Bersenang-senang sambil berolahraga di trampolin.",
                "tags" => ["Wisata Kota"],
                "review" => 2,
                "price" => "Rp 400.000",
                "images" => [
                    "https://happytour.id/img/smg/3Sam-Poo-kong.jpg",
                    "https://happytour.id/img/smg/2Candi-Gedong-Songo.jpg",
                    "https://happytour.id/img/smg/1lawang-sewu.jpg",
                    "https://happytour.id/img/smg/4umbul-sidomukti-kolam.jpg"
                ],
                "location" => "Semarang",
                "description"=> "Anda tak perlu khawatir akan bingung dan kesusahan mengurus segala hal yang berhubungan dengan perjalanan Anda di Semarang karena Anda menyewa jasa kami dan percaya pada kami untuk memberikan pengalaman wisata Semarang yang seru dan sesuai dengan impian Anda.",
                "iteneraries" => [
                    [
                        "title" => "Hari 1",
                        "summaries" => [
                            [
                                "time" => "2020-06-01 06:00:00",
                                "point" => "Bertemu di meeting point"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Menuju ke xxxx"
                            ]
                        ]
                    ],
                    [
                        "title" => "Hari 2",
                        "summaries" => [
                            [
                                "time" => "2020-06-01 06:00:00",
                                "point" => "Tiba di xxxxx"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Menxx xx  x x x xx x(opsional)"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Check-in, beristirahat & waktu bebas"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Menjelajahi area Candi A sdsxdsdsdsDA ASD "
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Menjelajahi DFGDFGsdfsdf sdf sdf sf sfsfsdfsd fs ng"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Kembali ke hotel, makan malam, waktu bebas & beristirahat"
                            ]
                        ]
                    ],
                    [
                        "title" => "Hari 3",
                        "summaries" => [
                            [
                                "time" => "2020-06-01 06:00:00",
                                "point" => "Wake up call & bersiap-siap"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Treksdsad adas fadas da sdasdasd asd"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Kembali ke hotel"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Tiba di hotel, sarapan, check out dan menuju aad ad"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Kembali ke Jakarta"
                            ],
                        ]
                    ]
                ]
            ]
        ];

        $data["3"] = [
            [
                "id"  => 15,
                "destination_id" => 3,
                "name" => "Solo City Tour Experience 10 Jam",
                "summary" => "Miliki sehari penuh wisata budaya Jawa dengan mengikuti tur ini, Pergi ke pasar loak Ngarsopuro untuk membeli barang antik seperti wayang, keramik, piringan hitam, dan masih banyak lagi, Lihat koleksi literatur lokal dan literatur Belanda yang berharga, termasuk gamelan, keris, dan wayang beber di Radya Pustaka Museum, Akhiri tur dengan mengunjungi Danar Hadi Batik Museum yang terkenal",
                "tags" => ["Wisata Kota"],
                "review" => 5,
                "price" => "Rp 638.750",
                "images" => [
                    "https://ik.imagekit.io/tvlk/xpe-asset/dsIfD0QxFcgaDmB6sQchobk5CmBu9PzsWhwFXGFxJ179jzSxIGG5kZNhhHY-p7nw/xxt/experience/image/IA-SMAILINGBALI-solo---mangkunegara-1d9d1e17-dc28-4daf-a68d-81fafba27392-jpg-1280x600-FIT-28abe3a51e910bbc9d853fa6e1a0da8e.jpeg?_src=imagekit&tr=q-60,c-at_max,w-720,h-512",
                    "https://ik.imagekit.io/tvlk/xpe-asset/dsIfD0QxFcgaDmB6sQchobk5CmBu9PzsWhwFXGFxJ179jzSxIGG5kZNhhHY-p7nw/xxt/experience/image/IA-SMAILINGBALI-solo---danar-hadi-3f060195-7ebb-469d-bb0f-327191ede963-jpg-1047x720-FIT-f42f47c2db66db0a2101d7ae171e32c5.jpeg?_src=imagekit&tr=q-60,c-at_max,w-296,h-325.3333333333333",
                    "https://ik.imagekit.io/tvlk/xpe-asset/dsIfD0QxFcgaDmB6sQchobk5CmBu9PzsWhwFXGFxJ179jzSxIGG5kZNhhHY-p7nw/xxt/experience/image/IA-SMAILINGBALI-solo---triwindu-8325ef81-dd92-4aff-bd3d-ac12e161a9e7-jpg-720x720-FIT-73fbe5b71da9d09721a306c529405315.jpeg?_src=imagekit&tr=q-60,c-at_max,w-296,h-325.3333333333333",
                    "https://ik.imagekit.io/tvlk/xpe-asset/dsIfD0QxFcgaDmB6sQchobk5CmBu9PzsWhwFXGFxJ179jzSxIGG5kZNhhHY-p7nw/xxt/experience/image/Museum-Radyapustaka-Surakarta01-jpg-966x720-FIT-7645844d0100c840b4d94fe0b23ea763.jpeg?_src=imagekit&tr=q-60,c-at_max,w-296,h-325.3333333333333"
                ],
                "location" => "Pasar Kliwon, Solo",
                "description"=> "Pengalaman yang Menanti Anda. Jika Anda ingin memiliki sehari penuh wisata budaya Jawa, maka Solo merupakan tempat sempurna untuk dikunjungi. Tur ini akan membawa Anda ke Istana Mangkunegaran sebagai destinasi pertama. Setelah makan siang, belilah beberapa barang antik seperti wayang, batik, keramik tua, piringan hitam, dan benda antik lainnya yang hanya dapat ditemukan di Pasar Loak Ngarsopuro. Sebelum mengakhiri tur di Danar Hadi Batik Museum, Anda akan mengunjungi Radya Pustaka Museum yang menempati gedung besar tua dengan gaya Jawa yang memamerkan koleksi berharga literatur lokal dan literatur Belanda. Di sana juga terdapat gamelan, keris, dan wayang beber yang merupakan bagian dari koleksi museum.",
                "iteneraries" => [
                    [
                        "title" => "Hari 1",
                        "summaries" => [
                            [
                                "time" => "2020-06-01 06:00:00",
                                "point" => "Penjemputan di hotel atau titik pertemuan lainnya"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Berkendara menuju Solo"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Mengunjungi Istana Mangkunegaran"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Makan siang"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Melanjutkan perjalanan ke Pasar Loak Ngarsopuro"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Melanjutkan perjalanan ke Radya Pustaka Museum"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Akhiri perjalanan dengan mengunjungi Danar Hadi Batik Museum"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Akhir tur"
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $data["4"] = [
            [
                "id"  => 17,
                "destination_id" => 4,
                "name" => "Tur Candi Borobudur, Prambanan 11 Jam",
                "summary" => "Jelajahi kekayaan alam dan budaya Yogyakarta bersama tur satu hari ini, Kunjungi Candi Borobudur yang merupakan candi Buddha terbesar di dunia, Kagumi keindahan detail Candi Prambanan yang tak tertandingi, Lengkapi perjalanan Anda dengan mengunjungi gunung Merapi dan menyaksikan pemandangan alam yang terbentuk dari erupsi gunung ini.",
                "tags" => ["Wisata Alam"],
                "review" => 5,
                "price" => "Rp 600.000",
                "images" => [
                    "https://ik.imagekit.io/tvlk/xpe-asset/dsIfD0QxFcgaDmB6sQchobk5CmBu9PzsWhwFXGFxJ179jzSxIGG5kZNhhHY-p7nw/xxt/experience/image/borobudur-temple--1--jpg-1080x720-FIT-a1d2b507f952daaeb2ff004010d77b41.jpeg?_src=imagekit&tr=q-60,c-at_max,w-720,h-512",
                    "https://ik.imagekit.io/tvlk/xpe-asset/dsIfD0QxFcgaDmB6sQchobk5CmBu9PzsWhwFXGFxJ179jzSxIGG5kZNhhHY-p7nw/xxt/experience/image/Merapi-Tour-1-jpg-960x720-FIT-aa4138a7eb98c47d635a75a65aa86413.jpeg?_src=imagekit&tr=q-60,c-at_max,w-296,h-244",
                    "https://ik.imagekit.io/tvlk/xpe-asset/dsIfD0QxFcgaDmB6sQchobk5CmBu9PzsWhwFXGFxJ179jzSxIGG5kZNhhHY-p7nw/xxt/experience/image/Merapi-Tour-2-jpg-965x720-FIT-0f5f24b8d2af4083a062377795f39c8d.jpeg?_src=imagekit&tr=q-60,c-at_max,w-296,h-244",
                    "https://ik.imagekit.io/tvlk/xpe-asset/dsIfD0QxFcgaDmB6sQchobk5CmBu9PzsWhwFXGFxJ179jzSxIGG5kZNhhHY-p7nw/xxt/experience/image/Prambanan-jpg-959x720-FIT-b08d7d632f71db172584d9b4e2d7f1c2.jpeg?_src=imagekit&tr=q-60,c-at_max,w-296,h-244"
                ],
                "location" => "Magelang",
                "description"=> "Pengalaman yang Menanti Anda, Jadikan liburan Anda di Yogyakarta semakin seru dengan bergabung dalam tur satu hari ini. Anda akan diajak menjelajahi setiap sudut yang mengagumkan di kota ini selama 11 jam. Kunjungi Candi Borobudur yang merupakan candi Buddha terbesar di dunia. Lalu, Anda akan mengunjungi Candi Prambanan, sebuah situs budaya lainnya yang begitu mengagumkan. Liburan Anda ke Yogyakarta tidaklah lengkap jika Anda tidak mengunjungi Gunung Merapi. Jangan khawatir, karena tur ini juga akan membawa Anda menyaksikan pemandangan indah dari gunung berapi tersebut. Apalagi yang Anda tunggu? Siapkan tas Anda dan nikmati petualangan terbaik dalam hidup Anda di Yogyakarta!",
                "iteneraries" => [
                    [
                        "title" => "Hari 1",
                        "summaries" => [
                            [
                                "time" => "2020-06-01 06:00:00",
                                "point" => "Penjemputan di bandara Adi Sucipto/Stasiun Jogja/hotel di sekitar kota Jogja"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Menuju ke Candi Borobudur"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Menjelajahi Candi Borobudur"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Menuju ke Wisata Merapi Lava Tour"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "MMakan siang"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Wisata Merapi Lava Tour"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Menuju Candi Prambanan"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Menjelajahi Candi Prambanan"
                            ],
                            [
                                "time" => "2020-06-01 09:00:00",
                                "point" => "Kembali ke hotel/stasiun/bandara Adi Sucipto"
                            ]
                        ]
                    ]
                ]
            ]
        ];

        return $data[$destination_id];
    }

    public function TravelDuringCorona(Request $request){
        $data['title']          = "Traveling During Corona Virus";
        $data['active_menu']    = 'home'; 
        $data['hotel_list']     = $this->hotelList($request->input("destination_id",1));
        $data['destinasions']   = $this->destinationModel();

        return view('landing-page.during-corona-page',$data);
    }

    public function TravelArticel(Request $request){
        $data['title']          = "Traveling During Corona Virus";
        $data['active_menu']    = 'home'; 
        $data['hotel_list']     = $this->hotelList($request->input("destination_id",1));
        $data['destinasions']   = $this->destinationModel();

        if($request->id == 1)
            return view('landing-page.articel-1',$data);

        if($request->id == 2)
            return view('landing-page.articel-2',$data);

        if($request->id == 3)
            return view('landing-page.articel-3',$data);

        if($request->id == 4)
            return view('landing-page.articel-4',$data);
    }

}
