<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 

Route::group(['namespace' => 'V1\Web' , 'middleware' => 'web'], function($router){
    Route::get('/',['as' => 'GetHomeControllerIndex' ,'uses' => 'HomeController@index']);

    // destination
    Route::get('/destination/{slug}/detail',['uses' => 'HomeController@DestinationDetail']);

    // accomodation
    Route::get('/accomodations',['uses' => 'HomeController@accomodationList']);
    
    Route::get('/accomodations/{destination_id}/{hotel_id}',['uses' => 'HomeController@accomodationDetail']);
    Route::get('/accomodations/{destination_id}/{hotel_id}/booking',['uses' => 'HomeController@accomodationbooking']);
    
    Route::get('/tours',['uses' => 'HomeController@toursList']);

    Route::get('/tours/{destination_id}/{tour_id}',['uses' => 'HomeController@tourDetail']);
    Route::get('/tours/{destination_id}/{tour_id}/booking',['uses' => 'HomeController@tourbooking']);


    Route::get('/travel-during-corona',['uses' => 'HomeController@TravelDuringCorona']);

    Route::get('/articel/{id}',['uses' => 'HomeController@TravelArticel']);


});