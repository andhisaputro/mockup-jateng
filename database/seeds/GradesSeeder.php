<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Grades;

class GradesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Grades::firstOrCreate(
            [
                "id"    => "38cc7acb-ae2a-11ea-8876-204747af83eb",
                "type"  => 1,
                "name"  => "Elementary School"
            ],
            [
                "created_at" => date("Y-m-d h:i:s"),
                "updated_at"  => date("Y-m-d h:i:s")
            ]
        );

        Grades::firstOrCreate(
            [
                "id"    => "757850ae-b5ba-11ea-a5a8-204747af83eb",
                "type"  => 2,
                "name"  => "Middle School"
            ],
            [
                "created_at" => date("Y-m-d h:i:s"),
                "updated_at"  => date("Y-m-d h:i:s")
            ]
        );

        Grades::firstOrCreate(
            [
                "id"    => "9ed7c935-b5ba-11ea-a5a8-204747af83eb",
                "type"  => 3,
                "name"  => "Height School"
            ],
            [
                "created_at" => date("Y-m-d h:i:s"),
                "updated_at"  => date("Y-m-d h:i:s")
            ]
        );
    }
}
