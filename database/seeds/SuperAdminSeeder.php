<?php

use Illuminate\Database\Seeder;
use App\Http\Models\User;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hasher   = app()->make('hash');
        $password = $hasher->make("12345678");

        User::firstOrCreate(
            [
                "role"  => "superadmin"
            ],
            [
                "title"      => 0,
                "username"   => "superadmin",
                "email"      => "admin@admin.com",
                "password"   => $password,
                "first_name" => "Super",
                "first_name" => "Admin",
                "created_at" => date("Y-m-d h:i:s"),
                "updated_at"  => date("Y-m-d h:i:s")
            ]
        );
    }
}
