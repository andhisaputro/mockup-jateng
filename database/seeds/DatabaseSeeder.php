<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SchoolYearSeeder::class);
        $this->call(GradesSeeder::class);
        $this->call(SuperAdminSeeder::class);
        $this->call(SchoolSeeder::class);
    }
}
