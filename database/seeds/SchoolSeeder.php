<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Schools;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Schools::firstOrCreate(
            [
                "id"        => "5bf8572d-ae2a-11ea-8876-204747af83eb",
                "grade_id"  => "38cc7acb-ae2a-11ea-8876-204747af83eb",
            ],
            [
                "name"      => "Sekolah Surya Bangsa (Elementary School)",
                "created_at" => date("Y-m-d h:i:s"),
                "updated_at"  => date("Y-m-d h:i:s")
            ]
        );

        Schools::firstOrCreate(
            [
                "id"        => "be2cd6fb-b633-11ea-a5a8-204747af83eb",
                "grade_id"  => "757850ae-b5ba-11ea-a5a8-204747af83eb",
            ],
            [
                "name"      => "Sekolah Surya Bangsa (Middle School)",
                "created_at" => date("Y-m-d h:i:s"),
                "updated_at"  => date("Y-m-d h:i:s")
            ]
        );

        Schools::firstOrCreate(
            [
                "id"        => "eccb1b81-b633-11ea-a5a8-204747af83eb",
                "grade_id"  => "9ed7c935-b5ba-11ea-a5a8-204747af83eb",
            ],
            [
                "name"      => "Sekolah Surya Bangsa (Height School)",
                "created_at" => date("Y-m-d h:i:s"),
                "updated_at"  => date("Y-m-d h:i:s")
            ]
        );

    }
}
