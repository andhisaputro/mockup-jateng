<?php

use Illuminate\Database\Seeder;
use App\Http\Models\SchoolYears;

class SchoolYearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $year_now = (int) date("Y");
        $year_last = $year_now + 100;
        $insert_data = [];
        for($i = $year_now ; $i < $year_last ; $i ++ ){
            $year_end = $i + 1; 
            $model = new SchoolYears;
            $save = $model->firstOrCreate(
                [
                    "year_from" => $i,
                    "year_to"   => $year_end,
                ],
                [
                    "id" => uuid(),
                    "created_at" => date("Y-m-d h:i:s"),
                    "updated_at"  => date("Y-m-d h:i:s")
                ]
            );
        }
    }
}
