 <table width="100%" cellpadding="12" cellspacing="0" border="0">
   <tbody>
      <tr>
         <td>
            <div style="overflow: hidden;">
               <font size="-1">
                  <u></u>
                  <div style="margin:0;padding:0" bgcolor="#FFFFFF">
                     <table width="100%" height="100%" style="min-width:348px" border="0" cellspacing="0" cellpadding="0" lang="en">
                        <tbody>
                           <tr height="32" style="height:32px">
                              <td></td>
                           </tr>
                           <tr align="center">
                              <td>
                                 <div>
                                    <div></div>
                                 </div>
                                 <table border="0" cellspacing="0" cellpadding="0" style="padding-bottom:20px;max-width:516px;min-width:220px">
                                    <tbody>
                                       <tr>
                                          <td width="8" style="width:8px"></td>
                                          <td>
                                             <div style="border-style:solid;border-width:thin;border-color:#dadce0;border-radius:8px;padding:40px 20px" align="center" class="m_8017229639576053870mdv2rw">
                                                <img src="https://sekolahsuryabangsa.sch.id/wp-content/uploads/2017/10/logo-sekolah-surya-bangsa-one-two-three.png" height="50" aria-hidden="true" style="margin-bottom:16px" alt="Google">
                                                <div style="font-family:Roboto,RobotoDraft,Helvetica,Arial,sans-serif;border-bottom:thin solid #dadce0;color:rgba(0,0,0,0.87);line-height:32px;padding-bottom:24px;text-align:center;word-break:break-word">
                                                   <div style="font-size:24px">
                                                   @section('title') 
                                                   @show 
                                                   </div>
                                                   <table align="center" style="margin-top:8px">
                                                      <tbody>
                                                         <tr style="line-height:normal">
                                                            <td><a style="font-family:Roboto,RobotoDraft,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.87);font-size:14px;line-height:20px">
                                                            @section('to') 
                                                            @show 
                                                            </a></td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </div>
                                                <div style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:14px;color:rgba(0,0,0,0.87);line-height:20px;padding-top:20px;text-align:center">
                                                   @section('content') 
                                                   @show 
                                                </div>
                                             </div>
                                             <div style="text-align:left">
                                                <div style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:11px;line-height:18px;padding-top:12px;text-align:center">
                                                   
                                                <span class="HOEnZb">
                                                      <font color="#888888">
                                                         <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="border-spacing:0">
                                                            <tbody> 
                                                               <tr>
                                                                  <td style="text-align:center;padding:10px 0px 20px 0px;font-family:sans-serif;font-size:12px;line-height:18px;color:#888888;border-collapse:collapse">
                                                                     <a href="http://do.co/email_twitter" target="_blank" ><img src="https://ci6.googleusercontent.com/proxy/gdn2n54DxJCmYNMt2JN9u4fgPE3uwrQoZESIsq5EIGVdpjwW4H-ucXWoJqNeb1F3cLlWs7tJX7J5XGeOD9od8IKUxXOqrJRtGw=s0-d-e1-ft#https://assets.digitalocean.com/email/twitter-icon.png" style="border:none" class="CToWUd"></a>&nbsp; &nbsp;
                                                                     <a href="http://do.co/email_gplus" target="_blank" ><img src="https://ci5.googleusercontent.com/proxy/GCBuM4hC_tYDH1TW_Vk8sH6bdmKoS-Dgp0eUTubNSVvqz7y51Yb1gG5_spUeK4TyGpBcAxnH_cI-z02KhIIP2MmO0tIcYFjXJM9Fyjk=s0-d-e1-ft#https://assets.digitalocean.com/email/google-plus-icon.png" style="border:none" class="CToWUd"></a>&nbsp; &nbsp;
                                                                     <a href="http://do.co/email_facebook" target="_blank" ><img src="https://ci4.googleusercontent.com/proxy/g92WSGwSlfUmUoaZeU4rd_dd5KA_etAyd8t-0F50DueozF8XRiVRHlb7QKg881oUEMF_AK-e2et5WX3_c42ReJ6a3-uK7Xf7Cjk=s0-d-e1-ft#https://assets.digitalocean.com/email/facebook-icon.png" style="border:none" class="CToWUd"></a>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </font>
                                                   </span>
                                                   <div style="direction:ltr">©&nbsp;<a class="m_8017229639576053870afal" style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:11px;line-height:18px;padding-top:12px;text-align:center">{{__("page.company.000001")}}</a></div>
                                                </div>
                                             </div>
                                          </td>
                                          <td width="8" style="width:8px"></td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </td>
                           </tr>
                           <tr height="32" style="height:32px">
                              <td></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </font>
            </div>
         </td>
      </tr>
   </tbody>
</table>