@extends ('email.layouts',array())
@section('content')
    <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff;border:1px solid #e5e5e5;border-spacing:0">
        <tbody>
            <tr>
                <td style="border-collapse:collapse">
                </td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #e5e5e5;border-collapse:collapse">
                <span class="HOEnZb"><font color="#888888">
                </font></span><span class="HOEnZb"><font color="#888888">
                </font></span>
                <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="border-spacing:0">
                    <tbody>
                        <tr>
                            <td style="padding:30px;font-family:sans-serif;font-size:16px;line-height:25px;color:#666666;border-collapse:collapse">
                            <strong>New Login Notification</strong>
                            <p>We noticed a login to your account <strong>{{$email}}</strong> from a new device. Was this you ?</p>
                            
                            <p>Device : <br> <strong>{{$user_agent}}</strong> : </p>

                            <p>IP Address : <br> <strong>{{$ip_address}}</strong> : </p>

                            <p><strong>If this was you ?.</strong><br>You can <i>ignore this message</i>. There's no need to take any action.</p>

                            <p><strong>If this wasn’t you ?.</strong><br>Secure your account with <a href="#">Change Your Password !</a></p>
                            
                            </td>
                        </tr>
                    </tbody>
                </table>
                <span class="HOEnZb"><font color="#888888">
                </font></span>
                </td>
            </tr>
        </tbody>
    </table>
@stop
