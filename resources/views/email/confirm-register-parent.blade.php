@extends ('email.layouts',array())

@section('title')
{{__("page.email.000002.001")}}
@stop
 
@section('content')
    <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="border-spacing:0">
        <tbody>
            <tr>
                <td style="padding:30px;font-family:sans-serif;font-size:16px;line-height:25px;color:#666666;border-collapse:collapse">
                <p>
                <strong>{{__("page.email.000002.002")}}</strong>
                <br><br>
                {{__("page.email.000002.003")}} 
                </p>
                <a href="{{env('APP_URL')}}/auth/login">{{__("page.email.000002.004")}} </a> 
                </td>
            </tr>
        </tbody>
    </table>
@stop
