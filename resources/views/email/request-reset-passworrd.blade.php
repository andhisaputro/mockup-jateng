@extends ('email.layouts',array())
@section('content')
    <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" style="background:#ffffff;border:1px solid #e5e5e5;border-spacing:0">
        <tbody>
            <tr>
                <td style="border-collapse:collapse">
                </td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #e5e5e5;border-collapse:collapse">
                <span class="HOEnZb"><font color="#888888">
                </font></span><span class="HOEnZb"><font color="#888888">
                </font></span>
                <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="border-spacing:0">
                    <tbody>
                        <tr>
                            <td style="padding:30px;font-family:sans-serif;font-size:16px;line-height:25px;color:#666666;border-collapse:collapse">
                            <strong>Reset Password</strong>
                            <p>If you requested a password reset for Lingker Account, click the button below. If you didn't make this request, ignore this email.</p>
                            <p><a style="display:block;text-align:center;background:#0069ff;padding:10px 15px;color:white;text-decoration:none;border-radius:3px" href="{{env('APP_SNAP_URL')}}/V1/password/reset/create?temporary_token={{$temporary_token}}" target="_blank" >Reset My Password</span></a></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <span class="HOEnZb"><font color="#888888">
                </font></span>
                </td>
            </tr>
        </tbody>
    </table>
@stop
