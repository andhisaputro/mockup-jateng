@extends ('landing-page.layouts',array())

@section('content')

<style>
.carousel-item {
    height: 20vh;
    min-height: 350px;
    background: no-repeat center center scroll;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
</style>
<header 
class="page-header page-header-small page-header-dark bg-img-cover"
style='background-image: url("{{url("/assets/image/home-page-1500x630.jpg")}}")'>
<div class="page-header-content">
    <div class="row justify-content-center">
        <div class="col-xl-8 col-lg-10 text-center">
            <h1 class="page-header-title" style="text-shadow: 0 0 2px #555;color: #fff !important;">VISIT JAWA TENGAH</h1>
            <p class="page-header-text mb-5" style="text-shadow: 0 0 2px #555;color: #fff !important;">Search for restaurants, contractors, and other local businesses in your area to find out which ones are the best!</p>
        </div>
    </div>
</div> 
</header>

<section>
    <div class="row">
        <div class="col-2 header-col-sec-small header-col-sec-1">&nbsp;
        </div>
        <div class="col header-col-sec-small header-col-sec-2">&nbsp;
        </div>
        <div class="col-6 header-col-sec-small header-col-sec-3">&nbsp;
        </div>
    </div>
</section>

<section class="bg-light py-10">
    <div class="container"> 
        <div class="row">
            <div class="col-12"> 
                <div class="card rounded-top bg-white p-4">
                <h1 class="mb-4">{{$hotel_detail['name']}}</h1>
                    <h5 class="">
                        <i class="fa fa-map-marker text-warning">&nbsp;</i>
                        &nbsp;Location Info
                    </h5> 
                    <p>{{$hotel_detail['address']}}</p>
                </div>
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li> 
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <!-- Slide One - Set the background image for this slide in the line below -->
                        @foreach ($hotel_detail['images'] as $image)
                            <div class="carousel-item active" style="background-image: url('{{$image}}')"> 
                            </div> 
                        @endforeach 
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>  
                <div class="bg-white container">                        
                        <div class="row">  
                        
                            <div class="col-md-6">
                                <h5 class="my-4 px-4">Nearby Places</h5>
                                <ul class="fa-ul mb-5">
                                    @foreach ($hotel_detail['nearby_places'] as $nearby_places)
                                        <li class="mb-3">
                                            <img width="20" src="{{$nearby_places['icon']}}" />
                                            <span class="fa-li"><i class="far fa-dot-circle text-primary-soft"></i></span>{{$nearby_places['title']}}
                                        </li>
                                    @endforeach     
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <h5 class="my-4 px-4">Popular in the Area</h5>
                                <ul class="fa-ul mb-5">
                                    @foreach ($hotel_detail['popular_area'] as $popular_area)
                                        <li class="mb-3">
                                            <img width="20" src="{{$popular_area['icon']}}" />
                                            <span class="fa-li"><i class="far fa-dot-circle text-primary-soft"></i></span>{{$popular_area['title']}}
                                        </li>
                                    @endforeach   
                                </ul>
                            </div>

                            <div class="hr hr-short hr-center   avia-builder-el-15  el_after_av_section  el_before_av_section  avia-builder-el-no-sibling "><span class="hr-inner "><span class="hr-inner-style"></span></span>
                            </div>

                            <div class="col-md-6">
                                <h5 class="my-4 px-4">General Facility</h5>
                                <ul class="fa-ul mb-5">
                                    @foreach ($hotel_detail['general_facility'] as $general_facility)
                                        <li class="mb-3">
                                            <span class="fa-li"><i class="far fa-dot-circle text-primary-soft"></i></span>{{$general_facility}}
                                        </li>
                                    @endforeach 
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <h5 class="my-4 px-4">Public Facility</h5>
                                <ul class="fa-ul mb-5">
                                    @foreach ($hotel_detail['public_facility'] as $public_facility)
                                        <li class="mb-3">
                                            <span class="fa-li"><i class="far fa-dot-circle text-primary-soft"></i></span>{{$public_facility}}
                                        </li>
                                    @endforeach 
                                </ul>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="col-12 card rounded-bottom bg-white" id="map" style="height:200px"> 
                </div> 

                <div class="my-4 shadow-none">  
                    @foreach ($hotel_detail['rooms'] as $room)
                        @if ($room['is_availble'])
                            <div class="bg-white mb-2 card-body d-flex align-items-center justify-content-between">
                                <p class="lead mb-0">{{$room['name']}}<br>
                                    <span class="text-warning">Rp&nbsp;<span class="h1 text-warning">{{$room['price']}}</span>
                                </p>
                                    <a class="btn btn-warning btn-marketing rounded-pill ml-2" href="{{url('accomodations/'.$hotel_detail['id'].'/'.$hotel_detail['hotel_id'].'/booking')}}">Booking</a>      
                            </div>  
                        @else
                            <div class="bg-white mb-2 card-body d-flex text-muted align-items-center justify-content-between">
                                <p class="lead mb-0">{{$room['name']}}<br>
                                    <span class="text-lighmutedt">Rp&nbsp;<span class="h1 text-muted">{{$room['price']}}</span>
                                </p>
                                <a class="btn btn-defauld bg-light disabled btn-marketing rounded-pill ml-2" href="#!">Full Book</a>
                            </div>
                        @endif
                    @endforeach 
                </div>  
            </div> 
            
        </div>
    </div> 
</section>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKVcJeJRmCqYxmVCKy0_leMwS4PKcKO20&callback=initMap"></script>
<script>
 function initMap() {
    // The location of Uluru
    var loc_items = [{"lat" : {{isset($hotel_detail['location']) ? $hotel_detail['location']['lat'] : 0}} , "lng" : {{isset($hotel_detail['location']) ? $hotel_detail['location']['long'] : 0}} }];
    var map = new google.maps.Map(document.getElementById('map'), 
        {
                center: loc_items[0],
                zoom: 16, 
                gestureHandling: 'cooperative'
        })
 }
</script>
@stop