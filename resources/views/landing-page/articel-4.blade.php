@extends ('landing-page.layouts',array())

@section('content')

<header 
class="page-header page-header-small page-header-dark bg-img-cover"
style='background-image: url("{{url("/assets/image/home-page-1500x630.jpg")}}")'>
<div class="page-header-content">
    <div class="row justify-content-center">
        <div class="col-xl-8 col-lg-10 text-center">
            <h1 class="page-header-title">VISIT JAWA TENGAH</h1>
            <p class="page-header-text mb-5">Search for restaurants, contractors, and other local businesses in your area to find out which ones are the best!</p>
        </div>
    </div>
</div> 
</header>
<section>
    <div class="row">
        <div class="col-2 header-col-sec-small header-col-sec-1">&nbsp;
        </div>
        <div class="col header-col-sec-small header-col-sec-2">&nbsp;
        </div>
        <div class="col-6 header-col-sec-small header-col-sec-3">&nbsp;
        </div>
    </div>
</section>
<section class="container py-10">
    <div id="main-content" role="main">
        <!--googleon: snippet-->
            
        <h1 class="my-10" style="font-size: 50px"> Share
            6 Guides on Being A More Responsible Traveler After the Pandemic</h1>
            <p>
                If you have added Indonesia to your post-pandemic bucket list, then you’re in for a good time! However, it might be a good idea to take an introspective look at yourself before you start visiting your favorite destinations. Being a responsible traveler will not only be good for you but also for the future of sustainable tourism. Looking for ways to maximize your holiday and contribute to sustainable tourism at the same time? These 6 tips will help you get started:
<br><br>
<strong>1</strong> | Embrace mindfulness



It is important to remember that after the pandemic, the world will be different. Therefore, make sure you are ready to adapt to new post-pandemic procedures once you resume traveling. Do some extensive research beforehand to learn about the new regulations and the post-pandemic conditions of the destination you’re about to visit.
 
<br><br>
<strong>2</strong> | Contribute to the local community


It’s okay to visit a destination just for fun, but you can add value to your holiday without reducing the excitement. Help the local industry grow by purchasing souvenirs made locally. If you seek authenticity, look no further than the self-sufficient local businesses and their well-made products.
 
<br><br>
<strong>3</strong> | Explore beyond the usual


Think outside the box. Go beyond the highly-crowded, mainstream tourist hotspots and discover new experiences. You can find out about these hidden gems by reading travel blogs or simply take a local’s advice and ask for a recommendation. Make a flexible schedule.
 
<br><br>
<strong>4</strong> | Delve into the local wisdom


When you’re visiting a destination, don’t forget to get some cultural insights from the locals. Dig deep into their incredible heritage and traditions to learn about their philosophy of life that might inspire you to become a better person and is worth sharing after you return home.
 
<br><br>
<strong>5</strong> | Love the environment


You travel to get the breath of fresh air, to witness the beautiful scenery, and to find the peace that’s hardly available in your city. Therefore, why not show your love for the environment by doing little things that contribute to its sustainability? Travel in an eco-friendly manner; carry your own tumbler, bring your own reusable cutlery, and more. Lastly, avoid littering. It’s the least you can do.
 
<br><br>
<strong>6</strong> | Prioritize safety and sanitation


Wherever you travel to, safety and sanitation should be your top priority. Bring your own hand sanitizers, masks, medicines, or anything that you find necessary to keep you clean and healthy. Practice physical distancing. Don’t ignore any symptom or discomfort that you experience during your trip. Reach out to the local authorities for immediate assistance.

 

By being a responsible traveler, you can become a better person and make the world a better place. Hopefully, the world will recover from the COVID-19 crisis soon. Until then, stay at home, pick a travel destination, plan your future trip, and, more importantly, become a better version of yourself.
            </p>
        </div>
</section>
 
@stop