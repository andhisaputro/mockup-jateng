<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>{{isset($title) ? $title : url('')}}</title>
        <link href="{{url('assets/sb-pro/css/styles.css')}}" rel="stylesheet" />
        <link href="{{url('assets/css/custome.css')}}" rel="stylesheet" />
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.png" />
        <script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.24.1/feather.min.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <div id="layoutDefault">
            <div id="layoutDefault_content">
                @include('landing-page.during-corona')
                <main>
                    <nav class="mt-4 navbar navbar-marketing navbar-expand-lg bg-transparent navbar-dark fixed-top">
                            <img style="margin-top: -25px;" class="logo-brand" src="{{url("/assets/image/logo.png")}}"/>
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i data-feather="menu"></i></button>
                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav ml-auto mr-lg-5">
                                    <li class="nav-item"><a class="nav-link active" href="{{url('/')}}">Welcome </a></li>
                                    <li class="nav-item dropdown dropdown-xl no-caret">
                                        <a class="nav-link dropdown-toggle" id="navbarDropdownDemos" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Region<i class="fas fa-chevron-right dropdown-arrow"></i></a>
                                        <div class="dropdown-menu dropdown-menu-right animated--fade-in-up mr-lg-n15" aria-labelledby="navbarDropdownDemos">
                                            <div class="row no-gutters"> 
                                                <div class="col-lg-12 p-lg-5 bg-white">
                                                    <div class="row">
                                                        <div class="col-lg-4"> 
                                                            <h6 class="dropdown-header text-primary">Kota</h6>
                                                                <a class="dropdown-item" href="#">Magelang</a>
                                                                <a class="dropdown-item" href="#">Pekalongan</a>
                                                                <a class="dropdown-item" href="#">Salatiga</a>
                                                                <a class="dropdown-item" href="#">Surakarta</a>
                                                                <a class="dropdown-item" href="#">Semarang</a>
                                                            <div class="dropdown-divider border-0"></div> 
                                                        </div> 
                                                        <div class="col-lg-4"> 
                                                            <h6 class="dropdown-header text-primary">Kabupaten</h6>
                                                                <a class="dropdown-item" href="#">Pekalongan</a>
                                                                <a class="dropdown-item" href="#">Pemalang</a>
                                                                <a class="dropdown-item" href="#">Purbalingga</a>
                                                                <a class="dropdown-item" href="#">Purworejo</a>
                                                                <a class="dropdown-item" href="#">Rembang</a>
                                                                <a class="dropdown-item" href="#">Semarang</a>
                                                                <a class="dropdown-item" href="#">Sragen</a>
                                                                <a class="dropdown-item" href="#">Sukoharjo</a> 
                                                                <a class="dropdown-item" href="#">Tegal</a> 
                                                                <a class="dropdown-item" href="#">Temanggung</a> 
                                                                <a class="dropdown-item" href="#">Wonogiri</a> 
                                                            <div class="dropdown-divider border-0"></div> 
                                                        </div>
                                                        <div class="col-lg-4"> 
                                                            <div class="dropdown-divider border-0"></div>
                                                            <h6 class="dropdown-header text-primary"></h6> 
                                                            <a class="dropdown-item" href="#">Grobogan</a>
                                                            <a class="dropdown-item" href="#">Jepara</a>
                                                            <a class="dropdown-item" href="#">Karanganyar</a>
                                                            <a class="dropdown-item" href="#">Kebumen</a>
                                                            <a class="dropdown-item" href="#">Kendal</a>
                                                            <a class="dropdown-item" href="#">Klaten</a>
                                                            <a class="dropdown-item" href="#">Kudus</a>
                                                            <a class="dropdown-item" href="#">Magelang</a>
                                                            <a class="dropdown-item" href="#">Pati</a> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="nav-item dropdown dropdown-lg no-caret">
                                        <a class="nav-link dropdown-toggle" id="navbarDropdownPages" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Before You Go</a>
                                    </li> 
                                </ul>
                            </div> 
                    </nav>
                    @section('content') 
                    @show
                </main>
            </div>
            <div id="layoutDefault_footer">
                <footer class="footer pt-10 pb-5 mt-auto bg-dark footer-dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="footer-brand">Jawa Tengah</div> 
                                <div class="icon-list-social mb-5">
                                    <a class="icon-list-social-link" href="javascript:void(0);"><i class="fab fa-instagram"></i></a><a class="icon-list-social-link" href="javascript:void(0);"><i class="fab fa-facebook"></i></a><a class="icon-list-social-link" href="javascript:void(0);"><i class="fab fa-twitter"></i></a>
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
                                        <div class="text-uppercase-expanded text-xs mb-4">Product</div>
                                        <ul class="list-unstyled mb-0">
                                            <li class="mb-2"><a href="javascript:void(0);">Landing</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">Pages</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">Sections</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">Documentation</a></li>
                                            <li><a href="javascript:void(0);">Changelog</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
                                        <div class="text-uppercase-expanded text-xs mb-4">Technical</div>
                                        <ul class="list-unstyled mb-0">
                                            <li class="mb-2"><a href="javascript:void(0);">Documentation</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">Changelog</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">Theme Customizer</a></li>
                                            <li><a href="javascript:void(0);">UI Kit</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-6 mb-5 mb-md-0">
                                        <div class="text-uppercase-expanded text-xs mb-4">Includes</div>
                                        <ul class="list-unstyled mb-0">
                                            <li class="mb-2"><a href="javascript:void(0);">Utilities</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">Components</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">Layouts</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">Code Samples</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">Products</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">Affiliates</a></li>
                                            <li><a href="javascript:void(0);">Updates</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-6">
                                        <div class="text-uppercase-expanded text-xs mb-4">Legal</div>
                                        <ul class="list-unstyled mb-0">
                                            <li class="mb-2"><a href="javascript:void(0);">Privacy Policy</a></li>
                                            <li class="mb-2"><a href="javascript:void(0);">Terms and Conditions</a></li>
                                            <li><a href="javascript:void(0);">License</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-5" />
                        <div class="row align-items-center">
                            <div class="col-md-6 small">Copyright &copy; Dinas Pariwisata Jawa Tengah 2020</div>
                            <div class="col-md-6 text-md-right small">
                                <a href="javascript:void(0);">Privacy Policy</a>
                                &middot;
                                <a href="javascript:void(0);">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
