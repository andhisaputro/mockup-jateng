@extends ('landing-page.layouts',array())

@section('content')

<style>
.carousel-item {
    height: 20vh;
    min-height: 350px;
    background: no-repeat center center scroll;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}
</style>
<header 
class="page-header page-header-small page-header-dark bg-img-cover"
style='background-image: url("{{url("/assets/image/home-page-1500x630.jpg")}}")'>
<div class="page-header-content">
    <div class="row justify-content-center">
        <div class="col-xl-8 col-lg-10 text-center">
            <h1 class="page-header-title" style="text-shadow: 0 0 2px #555;color: #fff !important;">VISIT JAWA TENGAH</h1>
            <p class="page-header-text mb-5" style="text-shadow: 0 0 2px #555;color: #fff !important;">Search for restaurants, contractors, and other local businesses in your area to find out which ones are the best!</p>
        </div>
    </div>
</div> 
</header>

<section>
    <div class="row">
        <div class="col-2 header-col-sec-small header-col-sec-1">&nbsp;
        </div>
        <div class="col header-col-sec-small header-col-sec-2">&nbsp;
        </div>
        <div class="col-6 header-col-sec-small header-col-sec-3">&nbsp;
        </div>
    </div>
</section>

<section class="bg-light py-10">
    <div class="container"> 
        <div class="row">
            <div class="col-12"> 
                <div class="card rounded-top bg-white p-4">
                <h1 class="mb-4">{{$tour_detail->name}}</h1>
                    <h5 class="">
                        <i class="fa fa-map-marker text-warning">&nbsp;</i>
                        &nbsp;Location Info
                    </h5> 
                    <p>{{$tour_detail->location}}</p>
                </div>
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#c1" data-slide-to="0" class="active"></li> 
                        <li data-target="#c2" data-slide-to="1" class=""></li> 
                        <li data-target="#c3" data-slide-to="2" class=""></li> 
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <!-- Slide One - Set the background image for this slide in the line below -->
                        @foreach ($tour_detail->images as $image)
                            <div id="c3" class="carousel-item active" style="background-image: url('{{$image}}')"></div> 
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>  
                <div class="bg-white container">                        
                        <div class="row">  
                        
                            <div class="col-md-9">
                                <h5 class="my-4">Ringkasan</h5>
                                <p>{{$tour_detail->summary}}</p>
                            </div>
                            <div class="col-md-3 text-center">
                                <h5 class="mt-4">Mulai dari</h5>
                                <h4 class="text-primary">{{$tour_detail->price}}/Orang</h4>
                                <a href="{{url('tours/'.$tour_detail->destination_id.'/'.$tour_detail->id.'/booking')}}" class="btn btn-marketing rounded-pill btn-primary aos-init aos-animate">Join</a>
                            </div>

                            <div class="hr hr-short hr-center   avia-builder-el-15  el_after_av_section  el_before_av_section  avia-builder-el-no-sibling "></div>

                            <div class="col-md-12">
                                <h5 class="my-4">Rencana Perjalanan</h5>

                                
                                <ul class="nav nav-tabs" role="tablist">
                                    @php $class = ' active'; @endphp
                                    @foreach($tour_detail->iteneraries as $it)
                                        <li class="nav-item">
                                            <a class="nav-link{{$class}}" data-toggle="tab" href="#{{str_replace(' ','_',trim($it->title))}}" style="color:#666 !important;font-size:16pt;">{{$it->title}}</a>
                                        </li>
                                        @php $class = ''; @endphp
                                    @endforeach
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    @php $class = ' active'; @endphp
                                    @foreach($tour_detail->iteneraries as $it)
                                        <div id="{{str_replace(' ','_',trim($it->title))}}" class="container tab-pane{{$class}}"><br>
                                            @foreach ($it->summaries as $su)
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        {{$su->time}}
                                                    </div>
                                                    <div class="col-md-9">{{$su->point}}</div>
                                                </div>
                                                <hr>
                                            @endforeach
                                        </div>
                                        @php $class = ''; @endphp
                                    @endforeach
                                </div>

                                <p>{{$tour_detail->description}}</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div> 
</section>
@stop