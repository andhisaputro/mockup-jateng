@extends ('landing-page.layouts',array())

@section('content')

<header 
class="page-header page-header-small page-header-dark bg-img-cover"
style='background-image: url("{{url("/assets/image/home-page-1500x630.jpg")}}")'>
<div class="page-header-content">
    <div class="row justify-content-center">
        <div class="col-xl-8 col-lg-10 text-center">
            <h1 class="page-header-title">VISIT JAWA TENGAH</h1>
            <p class="page-header-text mb-5">Search for restaurants, contractors, and other local businesses in your area to find out which ones are the best!</p>
        </div>
    </div>
</div> 
</header>

<section>
    <div class="row">
        <div class="col-2 header-col-sec-small header-col-sec-1">&nbsp;
        </div>
        <div class="col header-col-sec-small header-col-sec-2">&nbsp;
        </div>
        <div class="col-6 header-col-sec-small header-col-sec-3">&nbsp;
        </div>
    </div>
</section>

<section class="bg-light py-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <a class="card post-preview lift h-100" href="#!">
                    <img class="card-img-top" src="{{$hotel_detail['image']}}" alt="{{$hotel_detail['name']}}">
                    <div class="card-body">
                        <h5 class="card-title">{{$hotel_detail['name']}}</h5>
                        <p class="card-text">
                            {{$hotel_detail['address']}}
                            <hr>
                            <div class="mb-1">
                                <span>
                                    Durasi
                                    <span class="float-right">1 malam</span>
                                </span>
                            </div>
                            <hr>
                            <div class="mb-1">
                                <span>
                                    Check-in
                                    <span class="float-right">Jum, 18 Sep 2020</span>
                                </span>
                            </div>
                            <hr>
                            <div class="mb-1">
                                <span>
                                    Check-out
                                    <span class="float-right">Sab, 19 Sep 2020</span>
                                </span>
                            </div>
                            <hr>
                            <div class="mb-1">
                                <span>
                                    Tipe kamar
                                    <span class="float-right">Deluxe</span>
                                </span>
                            </div>
                            <hr>
                            <div class="mb-1">
                                <span>
                                    Jumlah kamar
                                    <span class="float-right">1 Kamar</span>
                                </span>
                            </div>
                            <hr>
                            <div class="mb-1">
                                <span>
                                    Tamu per kamar
                                    <span class="float-right">2 Tamu</span>
                                </span>
                            </div>
                            <hr>
                            <div class="mb-1">
                                <h5>
                                    Total harga
                                    <span class="float-right">Rp 1.000.000</span>
                                </h5>
                            </div>
                        </p>
                    </div>
                </a>
            </div>
            <div class="col-lg-8">
                <h1 class="mb-4 mt-3">Pemesanan Hotel</h1>
                <div class="card mb-5">
                    <div class="card-header">
                        <div class="mr-2 text-dark">
                            Data Pemesan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="text-dark" for="inputName">Nama kontak <span class="text-danger">*</span></label>
                            <input class="form-control py-4" id="inputName" type="text" placeholder="Nama Lengkap">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <label class="text-dark" for="inputhandphone">Handphone<span class="text-danger">*</span></label>
                                <input class="form-control py-4" id="inputhandphone" type="text" placeholder="Handphone ">
                            </div>
                            <div class="form-group col-md-7">
                                <label class="text-dark" for="inputemail">Email kontak <span class="text-danger">*</span></label>
                                <input class="form-control py-4" id="inputemail" type="text" placeholder="Email">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card mb-5">
                    <div class="card-header">
                        <div class="mr-2 text-dark">
                            Data Tamu (Optional)
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="text-dark">Nama Tamu 1</label>
                            <input class="form-control py-4" type="text" placeholder="Nama Tamu 1">
                        </div>
                        <div class="form-group">
                            <label class="text-dark">Nama Tamu 2</label>
                            <input class="form-control py-4" type="text" placeholder="Nama Tamu 2">
                        </div>
                    </div>
                </div>
                <div class="card mb-5">
                    <div class="card-header">
                        <div class="mr-2 text-dark">
                            Rincian harga
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="mb-1 text-dark">
                            {{$hotel_detail['name']}}
                        </div>
                        <hr>
                        <div class="mb-1">
                            <span>
                                (1x) Deluxe - Max Stay 13 Nights (1 malam)
                                <span class="float-right">Rp 826.446</span>
                            </span>
                        </div>
                        <hr>
                        <div class="mb-1">
                            <span>
                                Biaya Pemulihan Pajak
                                <span class="float-right">Rp 173.554</span>
                            </span>
                        </div>
                        <hr>
                        <div class="mb-1 text-dark">
                            <span>
                                TOTAL
                                <span class="float-right">Rp 1.000.000</span>
                            </span>
                        </div>
                        <hr>
                        <div class="d-flex justify-content-between">
                            <div>
                                Dengan mengeklik tombol "Booking", Anda menyetujui <a href="#" target="_blank">Syarat dan Ketentuan</a> serta <a href="#" target="_blank">Kebijakan Privasi</a>
                            </div>
                            <button class="btn btn-primary btn-lg mr-2">Booking</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>

@stop