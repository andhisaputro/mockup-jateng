@extends ('landing-page.layouts',array())

@section('content')

<header 
class="page-header page-header-small page-header-dark bg-img-cover"
style='background-image: url("{{url("/assets/image/home-page-1500x630.jpg")}}")'>
<div class="page-header-content">
    <div class="row justify-content-center">
        <div class="col-xl-8 col-lg-10 text-center">
            <h1 class="page-header-title">VISIT JAWA TENGAH</h1>
            <p class="page-header-text mb-5">Search for restaurants, contractors, and other local businesses in your area to find out which ones are the best!</p>
        </div>
    </div>
</div> 
</header>
<section>
    <div class="row">
        <div class="col-2 header-col-sec-small header-col-sec-1">&nbsp;
        </div>
        <div class="col header-col-sec-small header-col-sec-2">&nbsp;
        </div>
        <div class="col-6 header-col-sec-small header-col-sec-3">&nbsp;
        </div>
    </div>
</section>
<section class="container py-10">
    <div id="main-content" role="main">
        <!--googleon: snippet-->
            
        <h1 class="my-10" style="font-size: 50px">
            Make These Iconic Indonesia Sceneries As Your Online Meeting Virtual Background</h1>
            <p>
                Being the largest Buddhist temple in the world, the majestic Borobudur Temple Complex is a popular tourist attraction in Indonesia. Located in Magelang, Central Java, it covers an area of 123 x 123 meters, and is decorated with 2,672 relief panels and 504 Buddha statues. The bird’s-eye view from the top of this temple is amazing, but if you visit Borobudur when you #TravelTomorrow, then leave in the early morning from your inn and climb atop the temple to experience the mind-blowing sunrise. This cultural wonder will surely impress your colleagues if you add it as a virtual background for your online meetings.
            </p>

            <img src="https://www.indonesia.travel/content/dam/indtravelrevamp/en/trip-ideas/make-these-6-iconic-indonesia-sceneries-as-your-online-meeting-visiual-background/borbudur-image.jpg"/>
            <p>Getting to Borobudur is quite easy. It is only a one-hour drive from Yogyakarta. You can opt for a travel tour or you can rent a car from there.</p>
        </div>
</section>
 
@stop