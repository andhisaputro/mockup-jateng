@extends ('landing-page.layouts',array())

@section('content')

<header 
class="page-header page-header-small page-header-dark bg-img-cover"
style='background-image: url("{{url("/assets/image/home-page-1500x630.jpg")}}")'>
<div class="page-header-content">
    <div class="row justify-content-center">
        <div class="col-xl-8 col-lg-10 text-center">
            <h1 class="page-header-title" style="text-shadow: 0 0 2px #555;color: #fff !important;">VISIT JAWA TENGAH</h1>
            <p class="page-header-text mb-5" style="text-shadow: 0 0 2px #555;color: #fff !important;">Search for restaurants, contractors, and other local businesses in your area to find out which ones are the best!</p>
        </div>
    </div>
</div> 
</header>

<section>
    <div class="row">
        <div class="col-2 header-col-sec-small header-col-sec-1">&nbsp;
        </div>
        <div class="col header-col-sec-small header-col-sec-2">&nbsp;
        </div>
        <div class="col-6 header-col-sec-small header-col-sec-3">&nbsp;
        </div>
    </div>
</section>

<section class="bg-light py-10">
    <div class="container"> 
        <div class="row">
            <div class="col-lg-4 col-xl-3 mb-5">
            <form action="{{url('tours')}}">
                <div class="card bg-white">

                    <div class="card-header">                 
                        <p aria-expanded="true">Period<span> </span></p>
                    </div><!-- end card-header -->
                    
                    <div class="p-2">
                        Check-in
                        <input class="form-control form-control-solid" type="date" placeholder="Search . . ." value="{{date('d/m/Y')}}" aria-describedby="button-addon2">
                    </div><!-- end panel-collapse --> 

                    <div class="p-2">
                        Check-out
                        <input class="form-control form-control-solid" type="date" placeholder="Search . . ." value="{{date('d/m/Y')}}" aria-describedby="button-addon2">
                </div><!-- end panel-collapse --> 


                <div class="card-header">                 
                    <p aria-expanded="true">Select Destination <span> </span></p>
                    </div><!-- end card-header -->
                    
                    <div id="panel-1" style="">
                        <div class="card-body text-left">
                            <ul class="list-unstyled"> 
                                @foreach ($destinasions as $destinasion)
                                    <li class="custom-check">
                                        <input type="radio" {{request()->get('destination_id') == $destinasion['id']  ? 'checked' : ''}} value="{{$destinasion['id']}}" name="destination_id">
                                        <label><span></span>{{$destinasion['name']}}</label>
                                    </li> 
                                @endforeach 
                            </ul>
                        </div><!-- end card-body -->
                    </div><!-- end panel-collapse -->  
                
                    <div class="card-header py-2">                 
                        <button class="btn btn-info btn-block btn-marketing" type="submit">Search</button>
                    </div><!-- end card-header -->
                </form>
                </div>
            </div>
            <div class="col-lg-8 col-xl-9"> 

                <div class="card card-body mb-3">
                    <div class="row align-items-center">
                        <div class="col-lg-6">
                            <h4>Search your Destination</h4>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group mb-2">
                                <input class="form-control form-control-solid" type="text" placeholder="Search . . ." aria-label="Recipient's username" aria-describedby="button-addon2">
                                <div class="input-group-append">
                                <button class="btn btn-info" id="button-addon2" type="button">Search</button></div>
                            </div>
                        </div>
                    </div>
                </div>

                @foreach ($tour_list as $tr)
                    <div class="card bg-white mb-3">
                        <div class="row">
                            <div class="col-lg-3" style="background-image: url('{{$tr['images'][0]}}')">
                            
                            </div>
                            <div class="col-lg-9">
                                <div class="p-3">
                                    <h4>{{$tr['name']}}</h4>
                                    <p class="mb-3 lead text-gray mb-0">{{$tr['location']}}</p>
                                    <div class="pb- 3 text-yellow mb-1 pull-right">
                                        <div class="small mb-2">(18 Reviews)</div>
                                            @for ($i = 0; $i < $tr['review']; $i++)
                                                <i class="fas fa-thumbs-up"></i>                                        
                                            @endfor
                                        <a href="{{url('tours').'/'.$tr['destination_id'].'/'.$tr['id']}}" class="mb-2 float-right btn-warning btn rounded" type="submit">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                      
                @endforeach
                
                <nav aria-label="Page navigation example py-5">
                    <ul class="pagination pagination-blog justify-content-center">
                        <li class="page-item disabled">
                            <a class="page-link" href="#!" aria-label="Previous"><span aria-hidden="true">«</span></a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#!">1</a></li> 
                        <li class="page-item">
                            <a class="page-link" href="#!" aria-label="Next"><span aria-hidden="true">»</span></a>
                        </li>
                    </ul>
                </nav>

            </div>
        </div>
    </div> 
</section>
@stop