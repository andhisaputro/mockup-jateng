@extends ('landing-page.layouts',array())

@section('content')

<header 
class="page-header page-header-small page-header-dark bg-img-cover"
style='background-image: url("{{url("/assets/image/home-page-1500x630.jpg")}}")'>
<div class="page-header-content">
    <div class="row justify-content-center">
        <div class="col-xl-8 col-lg-10 text-center">
            <h1 class="page-header-title">VISIT JAWA TENGAH</h1>
            <p class="page-header-text mb-5">Search for restaurants, contractors, and other local businesses in your area to find out which ones are the best!</p>
        </div>
    </div>
</div> 
</header>

<section>
    <div class="row">
        <div class="col-2 header-col-sec-small header-col-sec-1">&nbsp;
        </div>
        <div class="col header-col-sec-small header-col-sec-2">&nbsp;
        </div>
        <div class="col-6 header-col-sec-small header-col-sec-3">&nbsp;
        </div>
    </div>
</section>

<section class="bg-light py-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h1 class="mb-4 mt-3">&nbsp;</h1>
                <a class="card post-preview lift h-100" href="#!">
                    <img class="card-img-top" src="{{$tour_detail->images[0]}}" alt="{{$tour_detail->name}}">
                    <div class="card-body">
                        <h5 class="card-title">{{$tour_detail->name}}</h5>
                        <p class="card-text">
                            {{$tour_detail->location}}
                            <hr>
                            <div class="mb-1">
                                <h5>
                                    DP (50%)
                                    <span class="float-right">Rp 200.000</span>
                                </h5>
                            </div>
                            <hr>
                            <div class="mb-1">
                                <h5>
                                    Harga Total
                                    <span class="float-right">Rp 400.000</span>
                                </h5>
                            </div>
                        </p>
                    </div>
                </a>
            </div>
            <div class="col-lg-8">
                <h1 class="mb-4 mt-3">Pemesanan Paket Tour</h1>
                <div class="card mb-5">
                    <div class="card-header">
                        <div class="mr-2 text-dark">
                            Data Pemesan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label class="text-dark" for="inputName">Nama kontak <span class="text-danger">*</span></label>
                            <input class="form-control py-4" id="inputName" type="text" placeholder="Nama Lengkap">
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <label class="text-dark" for="inputhandphone">Handphone<span class="text-danger">*</span></label>
                                <input class="form-control py-4" id="inputhandphone" type="text" placeholder="Handphone ">
                            </div>
                            <div class="form-group col-md-7">
                                <label class="text-dark" for="inputemail">Email kontak <span class="text-danger">*</span></label>
                                <input class="form-control py-4" id="inputemail" type="text" placeholder="Email">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card mb-5">
                    <div class="card-header">
                        <div class="mr-2 text-dark">
                            Rincian harga
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="mb-1 text-dark">
                            {{$tour_detail->name}}
                        </div>
                        <hr>
                        <div class="mb-1">
                            <span>
                                Jumlah Peserta
                                <span class="float-right">
                                <select class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                                </span>
                            </span>
                        </div>
                        <hr>
                        <div class="mb-1 text-dark">
                            <span>
                                TOTAL
                                <span class="float-right">Rp 1.000.000</span>
                            </span>
                        </div>
                        <hr>
                        <div class="d-flex justify-content-between">
                            <div>
                                Dengan mengeklik tombol "Join", Anda menyetujui <a href="#" target="_blank">Syarat dan Ketentuan</a> serta <a href="#" target="_blank">Kebijakan Privasi</a>
                            </div>
                            <button class="btn btn-primary btn-lg mr-2">Join</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</section>

@stop