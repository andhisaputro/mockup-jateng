<section class="bg-white pt-5 pb-10">
    <div class="container"> 
        
        <div class="my-5">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h2 class="mb-0">Activity {{isset($current_location) ? "In ".$current_location : ""}}</h2>
                <a class="btn btn-sm btn-primary d-inline-flex align-items-center" href="{{url('/tours')}}">See more<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right ml-1"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></a>
            </div>
        
            <div class="row">
                @foreach ($top3tours as $row)
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <a class="card lift" href="{{url('/tours')}}">
                            <img class="card-img-top" src="{{$row['images'][0]}}" alt="{{$row['name']}}" />
                            <div class="card-body text-center py-3">
                                <h6 class="card-title mb-0">{{$row['name']}}</h6>
                                <div class="text-yellow"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star-half-alt"></i></div>
                                <div class="small mb-2">({{$row['review']}} Reviews)</div>
                                <div class="small">{{$row['location']}}</div>
                            </div>
                            <div class="card-footer text-center text-xs"><i class="fas fa-store-alt mr-1"></i>Opened 2 weeks ago</div>
                        </a>
                    </div>
                @endforeach
            </div> 
        </div>

    </div> 
</section>