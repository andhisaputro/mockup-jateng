@extends ('landing-page.layouts',array())

@section('content')
<header 
class="page-header page-header-dark bg-img-cover"
style='background-image: url("{{$destinasion["image"]}}")'>
<div class="page-header-content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-10 text-center">
                <h1 class="page-header-title">
                    {{$destinasion["name"]}}
                </h1>
            </div>
        </div>
    </div>
</div> 
</header>
<section>
<div class="row">
    <div class="col-2 header-col-sec header-col-sec-1">
    </div>
    <div class="col header-col-sec header-col-sec-2">
    </div>
    <div class="col-6 header-col-sec header-col-sec-3">
    </div>
</div>
</section>
<section>
<div class="py-10 container">

    <div class="text-center mt-10">
        <h1 class="page-header-title">
            Discover&nbsp;{{$destinasion["name"]}}
        </h1>
        
        <div class="template-page content  av-content-full alpha units">
            <div class="post-entry post-entry-type-page post-entry-330">
                <div class="entry-content-wrapper clearfix">
                    <div class="hr hr-short hr-center   avia-builder-el-15  el_after_av_section  el_before_av_section  avia-builder-el-no-sibling "><span class="hr-inner "><span class="hr-inner-style"></span></span>
                    </div>
                </div>
            </div>
        </div>
        
    </div> 
</div>
</section>

<section class="container">
    <div class="single-post"> 
        
        <div class="row">
            @foreach ($destinasion["images"] as $item)
                <div class="col-md-6 mb-5">
                    <a class="card card-portfolio h-100" href="#!"><img class="card-img-top" src="{{$item["url"]}}" alt="...">
                    <div class="card-body"><div class="card-title">{{$item["name"]}}</div></div></a>
                </div>
            @endforeach
        </div>
</section>
<section class="container py-10 mb-10">
    <div class="container">
        <h1>{{$destinasion["name"]}}</h1>
        <p class="text-justify">{{$destinasion["description"]}}</p>
    </div> 
</section>

@include('landing-page.bussines-card')  

@stop