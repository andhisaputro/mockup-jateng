@extends ('landing-page.layouts',array())

@section('content')

<header 
class="page-header page-header-small page-header-dark bg-img-cover"
style='background-image: url("{{url("/assets/image/home-page-1500x630.jpg")}}")'>
<div class="page-header-content">
    <div class="row justify-content-center">
        <div class="col-xl-8 col-lg-10 text-center">
            <h1 class="page-header-title">VISIT JAWA TENGAH</h1>
            <p class="page-header-text mb-5">Search for restaurants, contractors, and other local businesses in your area to find out which ones are the best!</p>
        </div>
    </div>
</div> 
</header>
<section>
    <div class="row">
        <div class="col-2 header-col-sec-small header-col-sec-1">&nbsp;
        </div>
        <div class="col header-col-sec-small header-col-sec-2">&nbsp;
        </div>
        <div class="col-6 header-col-sec-small header-col-sec-3">&nbsp;
        </div>
    </div>
</section>
<section class="container py-10">
    <div id="main-content" role="main">
        <!--googleon: snippet-->
            
        <h1 class="my-10" style="font-size: 50px">We are Preparing to Welcome You Back Safely</h1>
            
        Indonesia has now entered the transitional phase of the Large-Scale Social Restrictions (PSBB) that had been implemented to counter the COVID-19 outbreak. Some of the public places have reopened with new protocols and we, too, are getting ready to safely welcome visitors back to Indonesia. Yes, we are taking measures to ensure the health and safety of our visitors, and here’s what we’ve done so far:
 
<br><br>
        <strong>1</strong> | Applying health protocols to your favorite destinations
        
        We are Preparing to Welcome You Back Safely to Indonesia
        
        We want you to confidently enjoy the beauty of Indonesia in a clean, healthy, and safe environment. Therefore, we are making sure that your favorite destinations are implementing Cleanliness, Health, and Safety protocols, which include the mandatory use of masks, provision of hand-washing facilities, spraying disinfectants, and conducting temperature checks for all visitors at every entrance.
         
        <br><br>
        <strong>2</strong> | Keeping you informed on current conditions
        
        We are Preparing to Welcome You Back Safely to Indonesia
        
        The website also provides recent FAQs, recent developments, and lists of reopened public places. Learning about these things beforehand would help avoid any inconvenience and make your trip pleasant when you visit us.
        <br><br>
        <strong>3</strong> | Adopting new protocols at our airports, harbors, and ground crossings
        
        We are Preparing to Welcome You Back Safely to Indonesia
        
        All sorts of entry points, such as airports, harbors, and ground crossings, are implementing cleanliness, health, and safety protocols, and new standard security routines. Kindly follow our new protocols to ensure a seamless travel experience on your visit to Indonesia.
         <br><br>
        
        <strong>4</strong> | Collaborating with hotels, transportations services, and local guides
        
        We are Preparing to Welcome You Back Safely to Indonesia
        
        In order to adjust to the “new normal”, we have partnered up with many accommodation and transportation providers to improve their quality standards and to make your stay more comfortable and safe. The hotels, transportation services, and local guides will follow the same strict safety protocols such as temperature checks, provision of hand-washing facilities, spraying disinfectants, and others.
         
        <br><br>
        <strong>5</strong> | Setting up health facilities
        
        We are Preparing to Welcome You Back Safely to Indonesia
        
        We are also setting up health facilities and medical stations within easy access from the tourist destinations to make you feel more secure with medical assistance available near you in case of any emergency.
        
        We know that some of you can’t wait to explore the beauty of Indonesia once it becomes safe to travel again. Therefore, we are doing our best to ensure that you are able to enjoy the highest standards of hygiene and safety on your future trip to Indonesia. Be inspired; #DreamNow and #TravelTomorrow.
        
         
        </div>
</section>
 
@stop