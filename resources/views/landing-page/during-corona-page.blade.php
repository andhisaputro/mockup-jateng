@extends ('landing-page.layouts',array())

@section('content')

<header 
class="page-header page-header-small page-header-dark bg-img-cover"
style='background-image: url("{{url("/assets/image/home-page-1500x630.jpg")}}")'>
<div class="page-header-content">
    <div class="row justify-content-center">
        <div class="col-xl-8 col-lg-10 text-center">
            <h1 class="page-header-title">VISIT JAWA TENGAH</h1>
            <p class="page-header-text mb-5">Search for restaurants, contractors, and other local businesses in your area to find out which ones are the best!</p>
        </div>
    </div>
</div> 
</header>
<section>
    <div class="row">
        <div class="col-2 header-col-sec-small header-col-sec-1">&nbsp;
        </div>
        <div class="col header-col-sec-small header-col-sec-2">&nbsp;
        </div>
        <div class="col-6 header-col-sec-small header-col-sec-3">&nbsp;
        </div>
    </div>
</section>
<section class="container py-10">
    <div id="main-content" role="main">
        <!--googleon: snippet-->
            
        <h1 class="my-10" style="font-size: 50px">Coronavirus travel advice</h1>
            
        <div class="auto-mobile"></div>
        <div class="by"><em><p>Need to travel during the COVID-19 outbreak? Protect yourself and others with these precautions.</p></em></div>
        <div class="auto">
            
        
         
        
        
        </div>
        
        <p>Are you considering rescheduling travel that you put off because of coronavirus disease 2019 (<abbr title="coronavirus disease 2019">COVID-19</abbr>)? Maybe you have work or family obligations that require you to travel. Yet worries about safe travel and lodging are holding you back.</p><div id="ad-mobile-top-container"></div>
        <p>Get the facts about your travel options and learn how to protect yourself if you must travel.</p>
        
        
            
        <div class="auto-mobile"></div>
        <h3>Stay safe when you travel</h3>
        <div class="auto">
            
        </div>
        
        <p>Travel in the U.S. is increasing as states ease restrictions put in place to slow the spread of the coronavirus. The Centers for Disease Control and Prevention (<abbr title="Centers for Disease Control and Prevention">CDC</abbr>) recommends following these steps to protect yourself and others when you travel:</p><div id="ad-mobile-top-container"></div>
        <ul>
            <li>Maintain a distance of 6 feet (2 meters) between you and others as much as possible.</li>
            <li>Avoid crowds.</li>
            <li>Wear a cloth face covering.</li>
            <li>Avoid touching your eyes, nose and mouth.</li>
            <li>Cover coughs and sneezes.</li>
            <li>Clean your hands often. It's especially important after going to the bathroom, before eating, and after coughing, sneezing or blowing your nose.
            <ul>
                <li>Wash your hands often with soap and water for at least 20 seconds.</li>
                <li>If soap and water aren't available, use a hand sanitizer that contains at least 60% alcohol. Cover all surfaces of your hands and rub your hands together until they feel dry.</li>
            </ul>
            </li>
        </ul>
        
        
            
        <div class="auto-mobile"></div>
        <h3>Check local coronavirus restrictions</h3>
        <div class="auto">
            
        </div>
        
        <p>Travel and other restrictions can vary from state to state. Save yourself unpleasant surprises and delays by checking for restrictions at your destination and anywhere you might stop along the way.</p>
        <p>State and local health department websites are your best resource. Keep in mind that restrictions can change rapidly depending on local conditions. Check back for updates as your trip gets closer.</p><div id="ad-mobile-top-container"></div>
        
        
            
        <div class="auto-mobile"></div>
        
        <div class="auto">
            
        </div>
        
        <p>While you're in research mode, look up visitor information and hours for businesses, restaurants parks and other places you may want to visit during your stay.</p>
        
        
            
        <div class="auto-mobile"></div>
        <h3>Air travel</h3>
        <div class="auto">
            
        </div>
        
        <p>Because of how air circulates and is filtered on airplanes, most viruses don't spread easily on flights. However, crowded flights make social distancing difficult. Plus air travel involves spending time in security lines and airport terminals, which can bring you in close contact with other people.</p><div id="ad-mobile-top-container"></div>
        <p>The <abbr title="Centers for Disease Control and Prevention">CDC</abbr> and the Federal Aviation Administration (FAA) have issued guidance to help airlines prevent the spread of the coronavirus. As a result, most major airlines in the U.S. require that crews and passengers wear cloth face coverings. To see what specific airports and airlines are doing to protect passengers, check their websites.</p>
        <p>The Transportation Security Administration (<abbr title="Transportation Security Administration">TSA</abbr>) has increased cleaning and disinfecting equipment and surfaces at screening checkpoints. If you haven't flown since the pandemic began, you'll notice some changes:</p>
        <ul>
            <li><abbr title="Transportation Security Administration">TSA</abbr> officers wearing masks and gloves, and practicing social distancing.</li>
            <li><abbr title="Transportation Security Administration">TSA</abbr> officers changing gloves after each pat-down.</li>
            <li>Plastic shields at document checking podium, bag search and drop off locations.</li>
            <li>Fewer travelers and, as a result, fewer open screening lanes.</li>
        </ul>
        <p>Also be aware that the <abbr title="Transportation Security Administration">TSA</abbr> has made a number of changes to the screening process:</p>
        <ul>
            <li>Travelers may wear masks during screening. However, <abbr title="Transportation Security Administration">TSA</abbr> employees may ask travelers to adjust masks for identification purposes. </li>
            <li>Instead of handing boarding passes to <abbr title="Transportation Security Administration">TSA</abbr> officers, travelers should place passes (paper or electronic) directly on the scanner and then hold them up for inspection.</li>
            <li>Each traveler may have one container of hand sanitizer up to 12 ounces (about 350 milliliters) in a carry-on bag. These containers will need to be taken out for screening.</li>
            <li>Food items should be transported in a plastic bag and placed in a bin for screening. Separating food from carry-on bags lessens the likelihood that screeners will need to open bags for inspection.</li>
            <li>Personal items such as keys, wallets and phones should be placed in carry-on bags instead of bins. This reduces the handling of these items during screening. </li>
        </ul>
        <p>Be sure to wash your hands with soap and water for at least 20 seconds directly before and after going through screening.</p>
        
        
            
        <div class="auto-mobile"></div>
        <h3>Car travel</h3>
        <div class="auto">
            
        </div>
        
        <p>Air travel might not be for you. You may prefer to drive, which also gives you more control over your environment. You'll still need to be smart about any stops you make, but that just takes some planning.</p>
        <p>Here are things to consider before you hit the road:</p>
        <ul>
            <li>Plan to make as few stops as possible, but stop driving if you become drowsy.</li>
            <li>Be sure to pack cloth face masks and alcohol-based hand sanitizer in an easily accessible spot so that you can use them during the trip as necessary.</li>
            <li>Prepare food and water to take on the trip. Consider including nonperishable items to tide you over in case access to restaurants and grocery stores is limited.</li>
            <li>Pack cleaning supplies, including a disinfectant and disposable gloves, if you'll be staying at a hotel or other lodging.</li>
            <li>When you need to get gas, use a disinfectant wipe on handles or buttons before you touch them. After fueling, use hand sanitizer. And when you get to where you're going, use soap and water to wash your hands for at least 20 seconds.</li>
            <li>If you choose to pick up a meal on the road, opt for restaurants that offer drive-thru or curbside service.</li>
        </ul><div id="ad-mobile-top-container"></div>
        
        
            
        <div class="auto-mobile"></div>
        <h3>Other ground transportation</h3>
        <div class="auto">
            
        </div>
        
        <p>If you travel by bus or train, be aware that sitting or standing within 6 feet (2 meters) of others for a prolonged period can put you at higher risk of getting or spreading the coronavirus. Follow the precautions outlined above for protecting yourself during travel.</p>
        <p>Even if you fly, you may need transportation once you arrive at your destination. You can investigate car rental options and their disinfection policies on the internet. If you plan to stay at a hotel, check into shuttle service availability.</p><div id="ad-mobile-top-container"></div>
        <p>If you plan to use a ride-hailing service, don't sit in the front seat near the driver. Consider handling your own bags during pickup and drop-off. Avoid coming into contact with frequently touched surfaces before cleaning them. If you'll be using public transportation, maintain social distancing, wear a mask, and use hand sanitizer or wash your hands after reaching your destination.</p>
        
        
            
        <div class="auto-mobile"></div>
        <h3>Hotels and other lodging</h3>
        <div class="auto">
            
        </div>
        
        <p>The hotel industry recognizes that travelers are concerned about the coronavirus and safety. Check any major chain's website for information about how it's protecting guests and staff. Some best practices include:</p>
        <ul>
            <li>Enhanced cleaning of public areas, elevators, guest rooms, as well as food preparation and laundry areas</li>
            <li>Social distancing measures in the lobby, at the front desk and in parking areas</li>
            <li>Masking of staff and guests</li>
            <li>Focused employee training in the following:
            <ul>
                <li>Hand-washing procedures</li>
                <li>Cleaning and disinfecting protocols</li>
                <li>Use of personal protective equipment</li>
            </ul><div id="ad-mobile-top-container"></div>
            </li>
            <li>Protocol in the event that a guest becomes ill, which should include temporarily closing the guest's room for cleaning and disinfecting</li>
        </ul>
        <p>For additional reassurance, call the hotel. Ask to be put in a room that has been vacant for at least 24 hours.</p>
        <p>Vacation rental websites, too, are upping their game when it comes to cleaning. They're highlighting their commitment to following public health guidelines, such as using masks and gloves when cleaning, and building in a waiting period between guests.</p>
        <p>Once you arrive at your room or rental, clean and disinfect all high-touch surfaces, such as doorknobs, light switches, countertops, tables, desks, phones, remote controls, toilets, sinks and faucets. Wash plates, glasses, cups and silverware (other than prewrapped plastic items) before using.</p>
        
        
            
        <div class="auto-mobile"></div>
        <h3>Make a packing list</h3>
        <div class="auto">
            
        </div>
        
        <p>When it's time to pack for your trip, grab any medicines you may need on your trip and these essential safe-travel supplies:</p>
        <ul>
            <li>Cloth face masks</li>
            <li>Alcohol-based hand sanitizer (at least 60% alcohol)</li>
            <li>Disinfectant wipes (at least 70% alcohol) for surfaces</li>
            <li>Thermometer</li>
        </ul><div id="ad-mobile-top-container"></div>
        
        
            
        <div class="auto-mobile"></div>
        <h3>Considerations for people at higher risk</h3>
        <div class="auto">
            
        </div>
        
        <p>Your risk of developing severe symptoms of <abbr title="coronavirus disease 2019">COVID-19</abbr> is higher if you're 65 or older or you have serious health problems, such as heart or lung conditions, a weakened immune system, diabetes, or severe obesity.</p>
        <p>If you're at higher risk, the <abbr title="Centers for Disease Control and Prevention">CDC</abbr> recommends avoiding crowds, cruise travel and nonessential air travel. If you must travel, talk with your doctor and ask about any additional precautions you may need to take.</p><div id="ad-mobile-top-container"></div>
        
        
            
        <div class="auto-mobile"></div>
        <h3>Remember safety first</h3>
        <div class="auto">
            
        </div>
        
        <p>Even the best plans may need to be set aside when illness strikes. If you feel sick before your planned travel, stay home except to get medical care.</p>
         
        </div>
</section>
 
@stop