@extends ('landing-page.layouts',array())

@section('content')

<header 
class="page-header page-header-small page-header-dark bg-img-cover"
style='background-image: url("{{url("/assets/image/home-page-1500x630.jpg")}}")'>
<div class="page-header-content">
    <div class="row justify-content-center">
        <div class="col-xl-8 col-lg-10 text-center">
            <h1 class="page-header-title">VISIT JAWA TENGAH</h1>
            <p class="page-header-text mb-5">Search for restaurants, contractors, and other local businesses in your area to find out which ones are the best!</p>
        </div>
    </div>
</div> 
</header>
<section>
    <div class="row">
        <div class="col-2 header-col-sec-small header-col-sec-1">&nbsp;
        </div>
        <div class="col header-col-sec-small header-col-sec-2">&nbsp;
        </div>
        <div class="col-6 header-col-sec-small header-col-sec-3">&nbsp;
        </div>
    </div>
</section>
<section class="container py-10">
    <div id="main-content" role="main">
        <!--googleon: snippet-->
            
        <h1 class="my-10" style="font-size: 50px">DESTINASI WISATA YG TELAH DI BUKA AREA JAWA TENGAH & DI YOGYAKARTA </h1>
            <p> 

                Salah satu stakeholder pariwisata adalah Asosiasi Experiential Learning Indonesia (AELI), dalam group whatsapp nya banyak memberikan informasi yang up to date terkait kegiatan maupun informasi terkait pariwisata. 

                Hari ini, 7 Juli 2020, informasi yang disampaikan secara berantai (diisi secara terbuka oleh siapa saja yang memiliki informasi) adalah daftar destinasi wisata Jawa Tengah dan DI Yogyakarta yang telah dibuka kembali:
                <br><br>
                1. Guci<br>
                2. Owabong<br>
                3. Sanggaluri Park<br>
                4. Purbasari Pancuran Mas<br>
                5. Baturraden
                6. Small World<br>
                7. The Villa<br>
                8. Kyai Langgeng<br>
                9. Candi Borobudur<br>
                10. Rafting Sungai Elo<br>
                11. Water park pantai widuri pemalang<br>
                12. Bukit igir kandang clekatakan pulosari pemalang<br>
                13. Bukit tangkeban pemalang.<br>
                14. Dusun sumiler<br>
                15. Saloka<br>
                16. Gedung Songo<br>
                17. Maerokoco<br>
                18. Sam Poo Kong<br>
                19. Masjid Agung Jawa Tengah,semarang<br>
                20. Eling Bening<br>
                21. Pantai Dewa Ruci (Purworejo)<br>
                22. Pantai Jetis (Purworejo)<br>
                23. Museum Tosan Aji ( Purworejo)<br>
                24. Pantai Ketawang (Purworejo)<br>
                25. Mangrove Demangedi (Purworejo)<br>
                26. Senjoyo (Salatiga) Citra Wisata<br>
                27. Kampung Seni dan budaya Gisikdrono Semarang<br>
                28. Herborist Semarang<br>
                29. Pantai Baron, Krakal, Kukup (Gunung Kidul)<br>
                30. Pantai Depok, Parangtritis, Pantai Goa Cemara (Bantul)<br>
                31. Ketep Pass<br>
                32. Goa Pindul<br>

            </p>
        </div>
</section>
 
@stop