<section class="bg-white pt-5 pb-10">
    <div class="container">
        <div class="card mt-n15 mb-10 z-1">
            <div class="card-body p-5">
                <div class="row align-items-center">
                    <div class="col-lg-6">
                        <h4>Get the latest Events</h4>
                        <p class="lead text-gray-500 mb-0">Stay in the loop with the latest Events !</p>
                    </div>
                    <div class="col-lg-6">
                        <div class="input-group mb-2">
                            <input class="form-control form-control-solid" type="text" placeholder="youremail@example.com" aria-label="Recipient's username" aria-describedby="button-addon2" />
                            <div class="input-group-append"><button class="btn btn-primary" id="button-addon2" type="button">Sign up</button></div>
                        </div>
                        <div class="small text-gray-500">You can unsubscribe at any time.</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="mb-5 text-center">
                    <div class="text-xs text-uppercase-expanded text-primary mb-2">Businesses</div>
                    <p class="lead mb-0">Here are some of the most popular Accomodation and Tours.</p>
                </div>
            </div>
        </div>
    
        <div class="my-5">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h2 class="mb-0">Accomodation {{isset($current_location) ? "In ".$current_location : ""}}</h2>
                <a class="btn btn-sm btn-primary d-inline-flex align-items-center" href="{{url('/accomodations')}}">See more<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right ml-1"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></a>
            </div>
        
            <div class="row">
                @foreach ($top3accomodation as $row)
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <a class="card lift" href="{{url('/accomodations/'.$row['id'].'/'.$row['hotel_id'])}}">
                            <img class="card-img-top" src="{{$row['image']}}" alt="{{$row['name']}}" />
                            <div class="card-body text-center py-3">
                                <h6 class="card-title mb-0">{{$row['name']}}</h6>
                                <div class="text-yellow">
                                    @for ($i = 0; $i < $row['star']; $i++)
                                        <i class="fas fa-star"></i>                                        
                                    @endfor
                                </div>
                                <div class="small mb-2">({{$row['review']}} Reviews)</div>
                                <div class="small">{{$row['address']}}</div>
                            </div>
                            <div class="card-footer text-center text-xs"><i class="fas fa-store-alt mr-1"></i>Opened 2 weeks ago</div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div> 
         
    
    </div>
    <div class="svg-border-angled text-dark">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none" fill="currentColor"><polygon points="0,100 100,0 100,100"></polygon></svg>
    </div>
    </section>