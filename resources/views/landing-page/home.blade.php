@extends ('landing-page.layouts',array())

@section('content')
<header 
class="home-header page-header page-header-dark bg-img width="50" class="rounded"-cover"
style='background-image: url("https://blog.airyrooms.com/wp-content/uploads/2018/07/800X500-3.png")'>
<div class="page-header-content">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-10 text-center">
                <h1 class="page-header-title" style="text-shadow: 0 0 2px #555;color: #fff !important;">VISIT JAWA TENGAH</h1>
                <p class="page-header-text mb-5" style="text-shadow: 0 0 2px #555;color: #fff !important;">Search for restaurants, contractors, and other local businesses in your area to find out which ones are the best!</p>
            </div>
        </div>
    </div>
</div> 
</header>
<section>
<div class="row">
    <div class="col-2 header-col-sec header-col-sec-1">
    </div>
    <div class="col header-col-sec header-col-sec-2">
    </div>
    <div class="col-6 header-col-sec header-col-sec-3">
    </div>
</div>
</section>
<section>
<div class="py-10 container">

    <div class="text-center mb-10">
        <h2>Explore</h2>
         
        <div class="template-page content  av-content-full alpha units">
            <div class="post-entry post-entry-type-page post-entry-330">
                <div class="entry-content-wrapper clearfix">
                    <div class="hr hr-short hr-center   avia-builder-el-15  el_after_av_section  el_before_av_section  avia-builder-el-no-sibling "><span class="hr-inner "><span class="hr-inner-style"></span></span>
                    </div>
                </div>
            </div>
        </div>
        
        <p class="lead">Search for Activity, Accomodation, and other local businesses.</p>
    </div>


    <div class="d-flex align-items-center justify-content-between mb-4">
        <h2 class="mb-0">Covid Update</h2>
        <a class="btn btn-sm btn-primary d-inline-flex align-items-center" href="#">See more<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right ml-1"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></a>
    </div>
     
    <div class="row my-10">
        <div class="col-lg-6 mb-5">
            <a class="d-flex h-100" href="{{url("articel/1")}}">
                <div class="icon-stack flex-shrink-0 bg-teal text-white">
                    <img width="80" class="rounded" src="https://www.indonesia.travel/content/dam/indtravelrevamp/en/trip-ideas/we-are-preparing-to-welcome-you-back-safely-to-indonesia/image2.jpg"/>
                </div>
                <div class="ml-4 px-5">
                    <h5 class="text">Region Avail to visit</h5>
                    <p class="text-50">We want you to confidently enjoy the beauty of Indonesia in a clean, healthy, and safe environment. Therefore, we are making . . . </p>
                </div>
            </a>
        </div>
        <div class="col-lg-6 mb-5">
            <a class="d-flex h-100" href="{{url("articel/2")}}">
                <div class="icon-stack flex-shrink-0 bg-teal text-white">
                    <img width="80" class="rounded" src="https://www.indonesia.travel/content/dam/indtravelrevamp/en/trip-ideas/we-are-preparing-to-welcome-you-back-safely-to-indonesia/image5.jpg"/>
                </div>
                <div class="ml-4 px-5">
                    <h5 class="text">We are Preparing to Welcome You Back Safely to Jawa Tengah</h5>
                    <p class="text-50">Although we are entering the transition period, your safety is still in our top priority.</p>
                </div>
            </a>
        </div>
        <div class="col-lg-6 mb-5 mb-lg-0">
            <a class="d-flex h-100" href="{{url("articel/3")}}">
                <div class="icon-stack flex-shrink-0 bg-teal text-white">
                    <img width="80" class="rounded" src="https://www.indonesia.travel/content/dam/indtravelrevamp/en/trip-ideas/make-these-6-iconic-indonesia-sceneries-as-your-online-meeting-visiual-background/thumbnail.jpg"/>
                </div>
                <div class="ml-4 px-5">
                    <h5 class="text">Make These Iconic Indonesia Sceneries As Your Online Meeting Virtual Background</h5>
                    <p class="text-50">Being the largest Buddhist temple in the world, the majestic Borobudur Temple Complex is a popular tourist attraction in Indonesia.</p>
                </div>
            </a>
        </div>
        <div class="col-lg-6">
            <a class="d-flex h-100" href="{{url("articel/4")}}">
                <div class="icon-stack flex-shrink-0 bg-teal text-white">
                    <img width="80" class="rounded" src="https://www.indonesia.travel/content/dam/indtravelrevamp/en/trip-ideas/6-guides-on-being-a-more-responsible-traveler-after-the-pandemic/i5.jpg"/>
                </div>
                <div class="ml-4 px-5">
                    <h5 class="text">6 Guides on Being A More Responsible Traveler After the Pandemic</h5>
                    <p class="text-50">If you have added Indonesia to your post-pandemic bucket list, then you’re in for a good time! However, it  . . .</p>
                </div>
            </a>
        </div>
    </div>
 

    @include('landing-page.actifity-card') 

    
    {{-- <div class="d-flex align-items-center justify-content-between mb-4">
        <h2 class="mb-0">Destinations</h2>
    </div>
    <div class="row">
        @foreach ($destinasions as $destinasion) 
            <div class="col-md-6 mb-5">
                <a class="card card-portfolio h-100" href="{{url('/destination/')."/".$destinasion['slug'].'/detail'}}"><img width="80" class="rounded" class="card-img width="50" class="rounded"-top" src="{{$destinasion["image"]}}" alt="...">
                <div class="card-body"><div class="card-title">{{$destinasion["name"]}}</div></div></a>
            </div>
        @endforeach 
    </div> --}}

    <div class="row">  
        @foreach ($destinasions as $destinasion) 
            <div class="col-md-6 mb-5">
                <a class="card card-portfolio h-100" href="{{url('/destination/')."/".$destinasion['slug'].'/detail'}}"><img class="card-img-top" src="{{$destinasion["image"]}}" alt="...">
                <div class="card-body"><div class="card-title">{{$destinasion["name"]}}</div></div></a> 
            </div>
        @endforeach 
    </div>

</div>
</section>
<section class="bg-light">
<div class="py-10 container">  
    <div class="d-flex align-items-center justify-content-between mb-4">
        <h2 class="mb-0">Next Events</h2>
        <a class="btn btn-sm btn-primary d-inline-flex align-items-center" href="#!">See more<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right ml-1"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></a>
    </div>
    <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-6 mb-4">
            <a class="card lift h-100" href="#!"><div class="card-flag card-flag-dark card-flag-top-right card-flag-lg">20 Jun 20</div>
                <img width="80" class="rounded" class="card-img width="50" class="rounded"-top max-h-150" src="https://visitjawatengah.jatengprov.go.id/assets/images/498c864b-cb49-4047-9c96-364eb956258f.jpg" alt="...">
                <div class="card-body p-3">
                    <div class="card-title small mb-0">Solo Great Sale</div>
                    <div class="text-xs text-gray-500">Solo,01-29 Feb 2020</div>
                </div></a>
        </div> 
        <div class="col-xl-4 col-lg-4 col-md-6 mb-4">
            <a class="card lift h-100" href="#!"><div class="card-flag card-flag-dark card-flag-top-right card-flag-lg">20 Jun 20</div>
                <img width="80" class="rounded" class="card-img width="50" class="rounded"-top max-h-150" src="https://visitjawatengah.jatengprov.go.id/assets/images/09ceb15f-e59a-45f0-98a0-7a4077ec4fd7.jpg" alt="...">
                <div class="card-body p-3">
                    <div class="card-title small mb-0">Hari Jadi Kabupaten Purworejo</div>
                    <div class="text-xs text-gray-500">Purworejo, 14 Feb - 14 Mar 2020</div>
                </div></a>
        </div> 
        <div class="col-xl-4 col-lg-4 col-md-6 mb-4">
            <a class="card lift h-100" href="#!"><div class="card-flag card-flag-dark card-flag-top-right card-flag-lg">20 Jun 20</div>
                <img width="80" class="rounded" class="card-img width="50" class="rounded"-top max-h-150" src="https://visitjawatengah.jatengprov.go.id/assets/images/7b09b41b-327c-4406-9da4-df66b1b922df.jpg" alt="...">
                <div class="card-body p-3">
                    <div class="card-title small mb-0">Pemalang Expo 2020</div>
                    <div class="text-xs text-gray-500">Peemalang, 16 Feb 2020</div>
                </div></a>
        </div> 
    </div>
</div>
</section> 

@include('landing-page.bussines-card') @stop