<style>
    #theTable {
      font-family: sans-serif, "Trebuchet MS", Arial, Helvetica;
      border-collapse: collapse;
      width: 100%;
      font-size: 10px;
    }
    
    #theTable td, #theTable th {
      border: 1px solid #ddd;
      padding: 8px;
    }
    
    #theTable tr:nth-child(even){background-color: #f2f2f2;}
    
    #theTable tr:hover {background-color: #ddd;}
    
    #theTable th {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #85898d;
      color: white;
      text-align: center;
    }
    </style>


<table id="theTable">
    <thead>
        <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Name</th> 
            <th colspan="3">In</th>
            <th colspan="3">Out</th>
            <th rowspan="2">Duration<br>(Minutes)</th>  
        </tr> 
        <tr> 
            <th>date</th>
            <th>Time</th>
            <th>Geoloc</th>
            <th>date</th>
            <th>Time</th>
            <th>Geoloc</th>
        </tr>
    </thead>
    <tbody>
        @php 
        $i = 1;
        @endphp
        @foreach($data as $item)
            <tr>  
                <td>{{$i++}}</td>
                <td>{{$item->User->first_name.' '.$item->User->last_name}}</td> 
                <td>{{date_time_format($item->date_time,'date')}}</td>
                <td>{{date_time_format($item->date_time,'time')}}</td>
                <td><a href="http://www.google.com/maps/place/{{$item->longitude.','.$item->latitude}}">{{$item->longitude.','.$item->latitude}}</a></td>  

                <td>{{$item->Child ? date_time_format($item->date_time,'date') : 'N/A'}}</td>
                <td>{{$item->Child ? date_time_format($item->date_time,'time') : 'N/A'}}</td>

                @if($item->Child)
                    <td><a href="http://www.google.com/maps/place/{{$item->Child->longitude.','.$item->Child->latitude}}">{{$item->Child ? $item->longitude.', '.$item->latitude : 'N/A'}}</a></td>
                @else
                    <td>N/A</td>
                @endif

                <td style="background-color: yellow">{{$item->Child ? duration($item->date_time,$item->Child->date_time,'minutes') : 0}}</td> 
            </tr> 
        @endforeach
    </tbody>
</table>