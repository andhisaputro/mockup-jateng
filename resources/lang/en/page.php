<?php 
return [
    "company" => [
        "000001" => "Sekolah Surya Bangsa",
        "000002" => "Sekolah Umum Surya Bangsa",
        "000003" => "Jl Boulevard Raya, Puri Beta 2, Larangan Ciledug Tangerang Telp : 021 73457888, 0812 90419917", 
        "000004" => "Copyright &copy; Sekolah Surya bangsa 2020",
        "000005" => "0878 89359850",
        "000006" => "info@sekolahsuryabangsa.sch.id"
    ],
    "general" => [
        "000001" => "more",
        "000002" => "meta",
        "000003" => "alamat",
        "000004" => "Kembali",
        "000005" => "Close",
        "000006" => "Kirim",
        "000007" => "Terima Kasih",
        "000008" => "Kelas",
        "000009" => "Gagal",
        "000010" => "Berhasil",
        "000011" => "Tabah",
        "000012" => "Update",
        "000013" => "Hapus"
    ],
    "menu" => [
        "000001" => "Home",
        "000002" => "Berita",
        "000003" => "Karya Murid",
        "000004" => "Contact Us", 
        "000005" => "Download Brosur",
        "000006" => "Silakan masukan alamat email dan nomor hanphone, Kami akan mengirimkan brosur ke alamat email anda"
    ], 
    "home" => [
        "000001" => "Sekolah Umum Surya Bangsa",
        "000002" => "Sesuai dengan namanya, Sekolah Umum Surya Bangsa adalah sekolah umum, yang menerima semua anak dengan latar belakang budaya, agama dan karakter anak yang berbeda. Oleh karenanya pembelajaran agama memfasilitasi semua agama yang dianut siswa. Filosofi kami adalah setiap anak berhak mendapatkan pendidikan yang berkualitas, menyeluruh dan terjangkau.  Oleh karenanya, kami tidak mengadakan tes penyaringan masuk sekolah. Bagi kami, semua anak seharusnya mendapatkan pendidikan yang baik.",
        "000003" => "Visi",
        "000004" => "Terwujudnya lingkungan belajar yang mampu mengaplikasikan ilmu Pengetahuan atau Pembelajaran menjadi nilai-nilai kehidupan atau nilai-nilai karakter peserta didik.",
        "000005" => "Misi",
        "000006" => [
            "001" => "Membentuk peserta didik yang kreatif, cerdas, mandiri, disiplin dan berkepribadian/ahklak mulia.",
            "002" => "Semua kelas melaksanakan pendekatan 'Active Learning' atau “pembelajaran aktif” pada semua mata pelajaran.",
            "003" => "Meningkatkan Potensi, kecerdasan dan minat sesuai dengan tingkat perkembangan dan kemampuan peserta didik ( Contoh : Program Bimbingan Karir, SBK dan Ekstra kurikuler)",
            "004" => "Mengembangkan budaya gemar membaca, yang dilakukan secara terprogram oleh wali kelas dan Guru Bahasa Indonesia.",
            "005" => "Menciptakan lingkungan sekolah yang aman, rapi, bersih, dan nyaman.",
            "006" => "Menciptakan suasana pembelajaran yang menyenangkan, komunikatif, tanpa takut salah, dan demokratis.",
            "007" => "Melaksanakan Pelajaran Tambahan kepada anak anak yang nilainya belum mencapai KKM.",
            "008" => "Menanamkan kepedulian sosial dan lingkungan, cinta damai, cinta tanah air, semangat kebangsaan, dan hidup demokratis.",
            "009" => "Menjelaskan Pengetahuan atau Informasi yang sedang relevan di kalangan masyarakat (Up to date dan kontekstual).",
        ],
        "000007" => "Metode Belajar",
        "000008" => "Melaksanakan proses kegiatan belajar mengajar berbasis ( TK ) Montessori dan Metode Sentra ( moving class), ( SD) Active Learning, (SMP-SMA) Transformational Knowledge to Wisdom.",
        "000009" => "Aplikasi Pendukung",
        "000010" => "Kegiatan Belajar Mengajar Efektif Dengan Aplikasi Terintegrasi",
        "000011" => "Guru",
        "000012" => "Guru Dapat Memantau Aktifitas Belajar Siswa Secara Realtime. Tugas harian dan materi ajar dapat ter-distribusi dengan baik",
        "000013" => "Siswa",
        "000014" => "Kemudahan berkomunikasi dengan Guru. Hasil tugas dapat terkirim tepat waktu", 
        "000015" => "Orang Tua",
        "000016" => "Membantu Peran Orang Tua Dalam Pengawasan Proses dan Hasil Belajar"
    ],
    "news" => [
        "000001" => "Browse articles, keep up to date, and learn more on our News!"
    ],
    "creation" => [ 
        "000001" => "Karya Murid Sekolah Surya Bangsa"
    ],
    "contact" => [
        "000001" => "Pendaftaran",
        "000002" => "Dapatkan informasi lebih banyak tentang pendaftaran Murid baru atau pindahan",
        "000003" => "Teknikal Support",
        "000004" => "Terkendala saat menggunnakan aplikasi kami. Silakan hubungi Teknikal Support kami",
        "000005" => "Kesiswaan",
        "000006" => "Bidang kesiswaan akan senang berkomunikasi dengan anda",
        "000007" => "Bantuan Umum",
        "000008" => "Untuk pertanyaan dukungan lainnya, silakan kirim email kepada kami di",
        "000009" => "Hubungi",
        "000010" => "Tidak dapat menemukan yang anda butuhkan ?",
        "000011" => "Isi form di bawah ini. Kami akan segera menghubungi anda"
    ],
    "form" => [
        "000001" => [
            "001" => "Alamat Email",
            "002" => "We'll never share your email with anyone else.",
            "003" => "email@example.com"
        ],
        "000002" => [
            "001" => "Nomor Handphone",
            "002" => "We'll never share your phone with anyone else.",
            "003" => "08**********" 
        ],
        "000003" => [
            "000001" => "Email Brosur Sudah Di Kirim Ke Alamat Email Anda",
            "000002" => "Data :type di Update", 
            "000003" => "Data :type di Input", 
            "000004" => "Data :type di Hapus",
            "000005" => "Data berhasil di Hapus", 
            "000006" => "Data berhasil di Input", 
            "000007" => "Data berhasil di Edit",
            "000008" => "Data gagal di Hapus", 
            "000009" => "Data gagal di Input", 
            "000010" => "Data gagal di Edit",
        ]
    ],
    "email" => [
        "000001" => [
          "001" => "Brosur Sekolah Surya bangsa",
          "002" => "Dengan Hormat",
          "003" => "Terima kasih Kami sampaikan atas kepercayaan Bapak/Ibu telah mengunjungi situs kami. Berikut kami lampirkan Brosur Sekolah Surya Bangsa dalam lampiran. Semoga kami dapat menjadi Pilihan terbaik untuk Putra/Putri Bapak/Ibu"
        ],
		"000002" => [
			"001" => "Pendaftaran Sekolah Surya Bangsa",
			"002" => "Dengan Hormat",
			"003" => "Terima kasih telah memilih Sekolah Surya Bangsa Untuk Pendidikan Putra Putri Anda. Anda dapat memantau proses Pendaftaran dengan login di dashboard Orang tua siswa di halaman ini",
			"004" => "Login Dashboard"
		]
    ],
    "online_register" => [
        "000001" => "Open Registration",
        "000002" => "Home Based School Integrated With Technology To Prepare Kids For The  Future",
        "000003" => "Form Registration",
        "000004" => "Student Data",
        "000005" => "Parents Data", 
        "000006" => "Document",
        "000007" => "Document 1",
        "000008" => "Document 2",
        "000009" => "Document 3",
        "000010" => "Transfer student",
        "000011" => "New student",
        "000012" => "SD",
        "000013" => "SMP",
        "000014" => "SMA"
    ],
	"user_sidebar" => [
		"000001" => "Active Child",
		"000002" => "Register",
		"000003" => "Report",
		"000004" => "Add Child",
		"000005" => "Home"
	]
];